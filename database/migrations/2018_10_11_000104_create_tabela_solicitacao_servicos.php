<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelaSolicitacaoServicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitacao_servicos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cliente');
            $table->string('servico')->nullable();
            $table->string('mensagem');
            $table->string('telefone');
            $table->string('endereco');
            $table->string('valor')->nullable();
            $table->string('despesa')->nullable();
            $table->string('horario')->nullable();
            $table->string('justificativa_descarte')->nullable();
            $table->string('dia')->nullable();
            $table->string('estado')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('cliente_id')->unsigned();
            $table->foreign('cliente_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitacao_servicos');
    }
}
