<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DenunciaSugestao extends Model
{
    protected $fillable = [
        'id',
        'usuario',
        'mensagem',
        'user_id'
    ];

    protected $table = 'denuncia_sugestoes';

    public $rules = [
        'mensagem' => 'required|max:255'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
