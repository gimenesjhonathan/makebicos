<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitacaoServico extends Model
{
    protected $fillable = [
        'id',
        'cliente',
        'servico',
        'mensagem',
        'telefone',
        'endereco',
        'valor',
        'despesa',
        'justificativa_descarte',
        'cliente_id',
        'user_id'
    ];

    public $rules = [
        'telefone' =>'required',
        'endereco' =>'required',
        'servico' => 'required',
        'mensagem' => 'required|min:20|max:250',
    ];

    protected $table = 'solicitacao_servicos';

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function cliente(){
        return $this->belongsTo(User::class, 'cliente_id');
    }

    public function avaliacao(){
        return $this->hasMany(Avaliacao::class, 'solicitacao_servico_id');
    }
}
