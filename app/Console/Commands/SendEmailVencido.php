<?php

namespace App\Console\Commands;

use App\Notifications\NotificacaoComentario;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SendEmailVencido extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendEmail:servicoVencido';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enviar Email dos servicos vencidos para o usuario';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $solicitacaoServicos = DB::table('solicitacao_servicos')
            ->where('estado', '=', 'vencido')
            ->get();

        foreach ($solicitacaoServicos as $solicitacaoServico){
            $user = $solicitacaoServico->user_id;

            $mensagem = 'Você tem solicitação de serviço que está vencida';
            $notificacao = array("usuario" => $mensagem, "url" => "/servicos-vencidos");
            User::find($user)->notify(new NotificacaoComentario($notificacao));
        }
    }
}
