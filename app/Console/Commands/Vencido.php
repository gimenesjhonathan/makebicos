<?php

namespace App\Console\Commands;

use App\Notifications\NotificacaoComentario;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class Vencido extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:venci';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'As datas que estao data vencidas sao alterar na tabela';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('solicitacao_servicos')
            ->where('estado', '=', 'marcado')
            ->where('dia', '<', date('Y-m-d'))
            ->update(['estado' => 'vencido']);
    }
}
