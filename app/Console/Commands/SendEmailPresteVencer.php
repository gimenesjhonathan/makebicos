<?php

namespace App\Console\Commands;

use App\Notifications\NotificacaoComentario;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SendEmailPresteVencer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:emailPreste';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia email para o usuário que ele tem serviço para atender hoje';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $solicitacaoServicos = DB::table('solicitacao_servicos')
            ->where('estado', '=', 'marcado')
            ->where('dia', '=', date('Y-m-d'))
            ->get();

        foreach ($solicitacaoServicos as $solicitacaoServico){
            $user = $solicitacaoServico->user_id;
            $horario = $solicitacaoServico->horario;

            $mensagem = 'Você tem solicitação de serviço marcada para hoje as '.$horario;
            $notificacao = array("usuario" => $mensagem, "url" => "/servicos-marcados");
            User::find($user)->notify(new NotificacaoComentario($notificacao));
        }
    }
}
