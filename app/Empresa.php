<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = [
        'id',
        'nome_empresa',
        'user_id',
    ];

    protected $table = 'empresas';

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
