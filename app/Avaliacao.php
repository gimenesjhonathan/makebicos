<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avaliacao extends Model
{
    protected $fillable = [
        'id',
        'cliente',
        'avaliacao',
        'user_id',
        'cliente_id',
        'solicitacao_servico_id'
    ];

    protected $table = 'avaliacoes';

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function cliente(){
        return $this->belongsTo(User::class, 'cliente_id');
    }

    public function servicoSolicitado(){
        return $this->belongsTo(SolicitacaoServico::class, 'solicitacao_servico_id');
    }
}
