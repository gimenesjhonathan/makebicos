<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resposta extends Model
{
    protected $fillable = [
      'id',
      'usuario',
      'resposta',
      'user_id',
      'comentario_id'
    ];

    protected $table = 'respostas';

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function comentario(){
        return $this->belongsTo(Comentario::class, 'comentario_id');
    }
}
