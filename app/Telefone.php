<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telefone extends Model
{
    protected $fillable = [
        'id',
        'fone',
        'user_id'
    ];

    protected $table = 'telefones';

    public $rules = [
        'fone' => 'required |min:14| max:15'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
