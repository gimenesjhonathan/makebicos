<?php

namespace App\Http\Controllers;

use App\Servico;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ServicoController extends Controller
{
    public function putAlterarServico(Request $request, $id)
    {
        $servico = Servico::find($id);
        $servico->desc_servico = $request->desc_servico;
        $servico->save();

        return redirect('/perfil')->with('message', 'Serviço alterado');

    }
    public function deleteServico($id)
    {
        $user_id = Servico::find($id)->user_id;
        $servico = User::find($user_id)->servico;

        if (count($servico) < 2) {
            return redirect('/perfil')->with('message', 'Você não pode excluir todos os serviços');

        }else {
            DB::table('servicos')->where('id', '=', $id)->delete();

            return redirect('/perfil')->with('message', 'Serviço excluido');
        }
    }
}
