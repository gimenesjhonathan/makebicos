<?php

namespace App\Http\Controllers;

use App\Endereco;
use App\User;
use Illuminate\Http\Request;
use Auth;


class EnderecoController extends Controller
{
    private $endereco;

    public function __construct(Endereco $endereco)
    {
        $this->endereco = $endereco;
    }

    public function messages(){
        return[
            'estado.required' => 'Campo Estado é Obrigatório',
            'estado.max' => 'Estado inválido',

            'cidade.required' => 'Campo Cidade é Obrigatório',
            'cidade.max' => 'Cidade inválido',

            'bairro.max' => 'Bairro inválido',

            'logradouro.max' => 'Logradouro inválido',

            'numero.max' => 'Logradouro inválido'
        ];
    }

    public function postAdicionaEndereco(Request $request){
        $userId = Auth::User()->id;
        $endereco = User::find($userId)->endereco;
        if(count($endereco) == 3){
            return redirect()->back()->with('message', 'O limite são 3 endereços');
        }

        $validate = validator($request->all(), $this->endereco->rules, $this->messages());

        if($validate->fails()){
            return redirect()->back()
                ->withErrors($validate)
                ->withInput();
        }

        if($request->estado && $request->cidade && $request->bairro && $request->logradouro && $request->numero){
            $endereco = new Endereco();
            $endereco->estado = $request->estado;
            $endereco->cidade = $request->cidade;
            $endereco->bairro = $request->bairro;
            $endereco->logradouro = $request->logradouro;
            $endereco->numero = $request->numero;
            $endereco->user_id = $userId;
            $endereco->save();
        }

        return redirect()->back()->with('message', 'Endereço Cadastrado');
    }
}
