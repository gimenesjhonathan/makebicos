<?php

namespace App\Http\Controllers;

use App\Telefone;
use App\User;
use Illuminate\Http\Request;
use Auth;


class TelefoneController extends Controller
{
    private $telefone;


    public function __construct(Telefone $telefone)
    {
        $this->telefone = $telefone;
    }

    public function messages(){
        return[
            'fone.required' => 'Campo telefone é obrigatório',
            'fone.min' => 'Telefone inválido',
            'fone.max' => 'Telefone inválido'
        ];
    }

    public function postAdicionaTelefone(Request $request){

        $validate = validator($request->all(), $this->telefone->rules, $this->messages());

        if($validate->fails()){
            return redirect()->back()
                ->withErrors($validate)
                ->withInput();
        }

        $userId = Auth::User()->id;
        $telefone = User::find($userId)->telefone;
        if(count($telefone) == 3){
            return redirect()->back()->with('message', 'O limite são 3 telefones');
        }

        if($request->fone){
            $telefone = new Telefone();
            $telefone->fone = $request->fone;
            $telefone->user_id = $userId;
            $telefone->save();
        }

        return redirect()->back()->with('message', 'Telefone Cadastrado');
    }
}
