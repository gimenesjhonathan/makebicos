<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\ListCategoria;
use App\Servico;
use App\User;
use App\Endereco;
use App\Telefone;
use Auth;
use App\Perfil;
use Illuminate\Support\facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriaController extends Controller
{
    private $endereco;
    private $telefone;
    private $servico;
    private $user;

    public function __construct(User $user, Endereco $endereco, Telefone $telefone, Servico $servico)
    {
        $this->user = $user;
        $this->endereco = $endereco;
        $this->telefone = $telefone;
        $this->servico = $servico;
    }

    public function getCategorias()
    {
        $categorias = Categoria::all();
        $servicos = " ";
        $id = " ";

        return view('/concluirs', compact('categorias','servicos', 'id'));
    }

    public function getServicos($id){
        $servicos = DB::table('list_categorias')
            ->where('categoria_id', '=', $id)
            ->get();

        $categorias = Categoria::all();

        return view('/concluirs', compact('servicos', 'categorias','id'));
    }

    public function getBuscarServico($id){
        $servicos = DB::table('list_categorias')
            ->where('categoria_id', '=', $id)
            ->get();

        $categorias = Categoria::all();

        return view('/buscar', compact('servicos', 'categorias','id'));
    }

    public function getBuscar()
    {
        $servicos = " ";
        $id = " ";

        $categorias = Categoria::all();
        return view('/buscar', compact('servicos', 'categorias','id'));
    }

    public function postPesquisar(Request $request)
    {
        $servico = $request->servico;
        $servicos = DB::table('servicos')->where('servico', $servico)->get();
        $usersId = $servicos->pluck('user_id');
        $users = User::find($usersId);

        return view('/lista', ['users' => $users]);
    }

    public function postCadastrarCategoria(Request $request){

        if($request->categoria){
            $categoria = new Categoria();
            $categoria->categoria = $request->categoria;
            $categoria->save();
        }

        return redirect()->back()->with('message', 'Categoria Cadastrado');
    }

    public function postCadastrarServico(Request $request){

        if($request->servico && $request->categoria_id){
            $servico = new ListCategoria();
            $servico->servico = $request->servico;
            $servico->categoria_id = $request->categoria_id;
            $servico->save();
        }

        return redirect()->back()->with('message', 'Serviço Cadastrado');
    }
}
