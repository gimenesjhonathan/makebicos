<?php

namespace App\Http\Controllers;

use App\Notifications\NotificacaoComentario;
use App\Servico;
use App\SolicitacaoServico;
use App\Telefone;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\DB;
use Khill\Lavacharts\Lavacharts;


class SolicitarServicoController extends Controller
{
    private $solicitacaoServico;


    public function __construct(SolicitacaoServico $solicitacaoServico)
    {
        $this->solicitacaoServico = $solicitacaoServico;
    }

    public function getSolicitar($id)
    {
        $userId = Auth::User()->id;

        if (count(User::find($userId)->endereco) == 0){
            return redirect('/concluir');
        }

        $userId = Auth::user()->id;
        $servicos = User::find($id)->servico;
        $telefones = User::find($userId)->telefone;
        $enderecos = User::find($userId)->endereco;

        return view("/solicitar", compact('id', 'servicos', 'telefones', 'enderecos'));
    }

    public function messages()
    {
        return [
            'telefone.required' => 'Marque um telefone',
            'endereco.required' => 'Marque um endereço',
            'servico.required' => 'Escolha um serviço',

            'mensagem.required' => 'Campo Descrição do seu Serviço é Obrigatório',
            'mensagem.min' => 'Campo descrição do seu Serviço o mínimo 20 dígitos',
            'mensagem.max' => 'Campo descrição do seu Serviço o máximo 250 dígitos'
        ];
    }

    public function postCadastrarSolicitacao(Request $request, $id)
    {

        $users = User::all();
        $name = Auth::user()->name;
        $sobrenome = Auth::user()->sobrenome;
        $nomeCompleto = $name . " " . $sobrenome;
        $userId = Auth::user()->id;

        $validate = validator($request->all(), $this->solicitacaoServico->rules, $this->messages());

        if ($validate->fails()) {
            return redirect()->back()
                ->withErrors($validate)
                ->withInput();
        }

        $solicitacaoServicoClientes = User::find($userId)->solicitacaoServicoCliente;

        if (count($solicitacaoServicoClientes) == 0) {
            if ($request->servico && $request->mensagem && $request->telefone && $request->endereco) {
                $solicitacaoServico = new SolicitacaoServico();
                $solicitacaoServico->cliente = $nomeCompleto;
                $solicitacaoServico->servico = $request->servico;
                $solicitacaoServico->mensagem = $request->mensagem;
                $solicitacaoServico->telefone = $request->telefone;
                $solicitacaoServico->endereco = $request->endereco;
                $solicitacaoServico->estado = 'novo';
                $solicitacaoServico->user_id = $id;
                $solicitacaoServico->cliente_id = $userId;
                $solicitacaoServico->save();

                $mensagem = $nomeCompleto.' Solicitou um serviço';
                $notificacao = array("usuario" => $mensagem, "url" => "/servicos");
                User::find($id)->notify(new NotificacaoComentario($notificacao));

                return redirect('/servicos-solicitados')->with('message', 'Serviço Cadastrado');
            }
        }
        $conferes = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $id)
            ->select('solicitacao_servicos.estado', 'solicitacao_servicos.user_id', 'cliente_id')
            ->get();

        foreach ($conferes as $confere) {
            if ($confere->cliente_id == $userId) {
                if ($confere->estado == 'marcado' || $confere->estado == 'novo') {
                    return redirect('/servicos-solicitados')->with('message', 'Aguarde você ja solicitou um serviço que não foi finalizado');
                }
            }
        }

        if ($request->servico && $request->mensagem && $request->telefone && $request->endereco) {
            $solicitacaoServico = new SolicitacaoServico();
            $solicitacaoServico->cliente = $nomeCompleto;
            $solicitacaoServico->servico = $request->servico;
            $solicitacaoServico->mensagem = $request->mensagem;
            $solicitacaoServico->telefone = $request->telefone;
            $solicitacaoServico->endereco = $request->endereco;
            $solicitacaoServico->estado = 'novo';
            $solicitacaoServico->user_id = $id;
            $solicitacaoServico->cliente_id = $userId;
            $solicitacaoServico->save();

            $mensagem = $nomeCompleto.' Solicitou um serviço';
            $notificacao = array("usuario" => $mensagem, "url" => "/servicos");
            User::find($id)->notify(new NotificacaoComentario($notificacao));

            return redirect('/servicos-solicitados')->with('message', 'Serviço Cadastrado');
        }

    }

    public function putCadastrarHorarioAtendimento(Request $request, $id)
    {

        if ($request->horario == null || $request->dia == null) {
            return redirect()->back()->with('message', 'Os campos Data e Horario são obrigatórios na marcação dos serviços');
        }
        if ($request->dia < date('Y-m-d')){
            return redirect()->back()->with('message', 'Data invalida');
        }

        if ($request->horario <= date('H:i') && $request->dia == date('Y-m-d')){
            return redirect()->back()->with('message', 'Data e hora invalida');
        }

        if ($request->horario && $request->dia) {

            $solicitacaoServico = SolicitacaoServico::find($id);
            $solicitacaoServico->horario = $request->horario;
            $solicitacaoServico->dia = $request->dia;
            $solicitacaoServico->estado = 'marcado';
            $solicitacaoServico->save();
        }
        $data = date("d/m/Y", strtotime($request->dia));
        $cliente_id = SolicitacaoServico::find($id)->cliente_id;
        $user_id = SolicitacaoServico::find($id)->user_id;
        $empresa = User::find($user_id)->empresa;
        $nomeEmpresa = $empresa->first()->nome_empresa;

        $mensagem = 'Prestação de servicos da '.$nomeEmpresa.' ficou marcado para o dia '.$data.' as '.$request->horario;
        $notificacao = array("usuario" => $mensagem, "url" => "/servicos-solicitados");
        User::find($cliente_id)->notify(new NotificacaoComentario($notificacao));

        return redirect('/servicos')->with('message', 'Serviço marcado');
    }

    public function getServicos()
    {
        $userId = Auth::user()->id;

        $solicitacaoServicos = DB::table('solicitacao_servicos')
            ->join('users', 'users.id', '=', 'solicitacao_servicos.user_id')
            ->where('solicitacao_servicos.user_id', '=', $userId)
            ->select('solicitacao_servicos.*')
            ->get();

        return view('/servicos', compact('solicitacaoServicos'));
    }

    public function getServicosMarcados()
    {
        $userId = Auth::user()->id;

        $solicitacaoServicos = DB::table('solicitacao_servicos')
            ->join('users', 'users.id', '=', 'solicitacao_servicos.user_id')
            ->where('solicitacao_servicos.user_id', '=', $userId)
            ->select('solicitacao_servicos.*')
            ->get();

        return view('/servicos-marcados', compact('solicitacaoServicos'));
    }

    public function getServicosConcluidos()
    {
        $userId = Auth::user()->id;

        $solicitacaoServicos = DB::table('solicitacao_servicos')
            ->join('users', 'users.id', '=', 'solicitacao_servicos.user_id')
            ->where('solicitacao_servicos.user_id', '=', $userId)
            ->select('solicitacao_servicos.*')
            ->get();

        return view('/servicos-concluidos', compact('solicitacaoServicos'));
    }

    public function getServicosVencidos()
    {
        $userId = Auth::user()->id;

        $solicitacaoServicos = DB::table('solicitacao_servicos')
            ->join('users', 'users.id', '=', 'solicitacao_servicos.user_id')
            ->where('solicitacao_servicos.user_id', '=', $userId)
            ->select('solicitacao_servicos.*')
            ->get();

        return view('/servicos-vencidos', compact('solicitacaoServicos'));
    }

    public function getServicosDescartados()
    {
        $userId = Auth::user()->id;

        $solicitacaoServicos = DB::table('solicitacao_servicos')
            ->join('users', 'users.id', '=', 'solicitacao_servicos.user_id')
            ->where('solicitacao_servicos.user_id', '=', $userId)
            ->select('solicitacao_servicos.*')
            ->get();

        return view('/servicos-descartados', compact('solicitacaoServicos'));
    }


    public function putConcluirServico(Request $request, $id)
    {

        if ($request->valor == null || $request->despesa == null) {
            return redirect()->back()->with('message', 'Os campos Valor e Despesas são obrigatório para conclusão do serviço');
        }

        if ($request->valor && $request->despesa) {
            $valor = str_replace(',', '.', $request->valor);
            $despesa = str_replace(',', '.', $request->despesa);
            $solicitacaoServico = SolicitacaoServico::find($id);
            $solicitacaoServico->estado = 'concluido';
            $solicitacaoServico->valor = $valor;
            $solicitacaoServico->despesa = $despesa;
            $solicitacaoServico->save();
        }

        $cliente_id = SolicitacaoServico::find($id)->cliente_id;
        $user_id = SolicitacaoServico::find($id)->user_id;
        $empresa = User::find($user_id)->empresa;
        $nomeEmpresa = $empresa->first()->nome_empresa;
        $mensagem = 'Avalie os serviços prestados pela '.$nomeEmpresa;
        $notificacao = array("usuario" => $mensagem, "url" => "/servicos-solicitados");
        User::find($cliente_id)->notify(new NotificacaoComentario($notificacao));

        return redirect('/servicos')->with('message', 'Servico Concluído');
    }

    public function putDescartarServico(Request $request, $id)
    {

        if ($request->justificativa_descarte == null) {
            return redirect()->back()->with('message', 'Justifique o Descarte');
        }

        if ($request->justificativa_descarte) {
            $solicitacaoServico = SolicitacaoServico::find($id);
            $solicitacaoServico->estado = 'descartado';
            $solicitacaoServico->justificativa_descarte = $request->justificativa_descarte;
            $solicitacaoServico->save();
        }

        $cliente_id = SolicitacaoServico::find($id)->cliente_id;
        $user_id = SolicitacaoServico::find($id)->user_id;
        $empresa = User::find($user_id)->empresa;
        $nomeEmpresa = $empresa->first()->nome_empresa;
        $mensagem = 'O serviço que você solicitou para '.$nomeEmpresa.' foi cancelado pelo motivo '.$request->justificativa_descarte;
        $notificacao = array("usuario" => $mensagem, "url" => "/servicos-solicitados");
        User::find($cliente_id)->notify(new NotificacaoComentario($notificacao));

        return redirect('/servicos')->with('message', 'Servico Descartado');
    }

    public function getServicosSolicitados()
    {

        $userId = Auth::user()->id;

        $solicitacaoServicoClientes = DB::table('solicitacao_servicos')
            ->where('solicitacao_servicos.cliente_id', '=', $userId)
            ->select('solicitacao_servicos.*')
            ->orderBy('id', 'desc')
            ->paginate(10);

        return view('/servicos-solicitados', compact('solicitacaoServicoClientes', 'user'));
    }

    public function deleteServico($id)
    {
        $solicitacaoServico = SolicitacaoServico::find($id);

        $solicitacaoServico->delete();

        $user_id = $solicitacaoServico->user_id;
        $cliente = $solicitacaoServico->cliente;
        $mensagem = $cliente.' cancelou serviço que tinha solicitado';
        $notificacao = array("usuario" => $mensagem, "url" => "/servicos");

        User::find($user_id)->notify(new NotificacaoComentario($notificacao));
        return redirect('/servicos-solicitados')->with('message', 'Serviço excluido');
    }

    public function grafico()
    {
        $userId = Auth::user()->id;
        //Mês Atual
        $dateAtual = date('Y-m-d');
        $mesAtual = date('Y-m-01');
        $soMesAtual = date('m');

        $valorMesAtual = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->where('dia', '<=', $dateAtual)
            ->where('dia', '>=', $mesAtual)
            ->sum('valor');
        $despesaMesAtual = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->where('dia', '<=', $dateAtual)
            ->where('dia', '>=', $mesAtual)
            ->sum('despesa');
        $lucroMesAtual = $valorMesAtual-$despesaMesAtual;
        $replaceLucrosMesAtual = number_format($lucroMesAtual, 2, ',', ' ');
        $replaceDespesaMesAtual = number_format($despesaMesAtual, 2, ',', ' ');
        $replaceValorMesAtual = number_format($valorMesAtual, 2, ',', ' ');


        //Mês Passado
        $dateInicio = mktime (0, 0, 0, date("m")-1, date("01"),  date("Y"));
        $dateFim = mktime (0, 0, 0, date("m")-1, date("31"),  date("Y"));

        $dateInicioMes = date('Y-m-d', $dateInicio);
        $dateFimMes = date('Y-m-d', $dateFim);
        $soMesPassado = date('m', $dateInicio);

        $valorMesPassado = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->where('dia', '<=', $dateFimMes)
            ->where('dia', '>=', $dateInicioMes)
            ->sum('valor');
        $despesaMesPassado = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->where('dia', '<=', $dateFimMes)
            ->where('dia', '>=', $dateInicioMes)
            ->sum('despesa');
        $lucroMesPassado = $valorMesPassado-$despesaMesPassado;

        //Mês Ante-Passado
        $dateInicioAnte = mktime (0, 0, 0, date("m")-2, date("01"),  date("Y"));
        $dateFimAnte = mktime (0, 0, 0, date("m")-2, date("31"),  date("Y"));

        $mesAnteInicio = date('Y-m-d', $dateInicioAnte);
        $mesAnteFim = date('Y-m-d', $dateFimAnte);
        $soMesAnte = date('m', $dateInicioAnte);

        $valorMesAntePassado = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->where('dia', '<=', $mesAnteFim)
            ->where('dia', '>=', $mesAnteInicio)
            ->sum('valor');
        $despesaMesAntePassado = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->where('dia', '<=', $mesAnteFim)
            ->where('dia', '>=', $mesAnteInicio)
            ->sum('despesa');
        $lucroMesAntePassado = $valorMesAntePassado-$despesaMesAntePassado;

        //Mês Rere-Passado
        $dateInicioRere = mktime (0, 0, 0, date("m")-3, date("01"),  date("Y"));
        $dateFimRere = mktime (0, 0, 0, date("m")-3, date("31"),  date("Y"));

        $mesRereInicio = date('Y-m-d', $dateInicioRere);
        $mesRereFim = date('Y-m-d', $dateFimRere);
        $soRere = date('m', $dateInicioRere);

        $valorMesRerePassado = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->where('dia', '<=', $mesRereFim)
            ->where('dia', '>=', $mesRereInicio)
            ->sum('valor');
        $despesaMesRerePassado = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->where('dia', '<=', $mesRereFim)
            ->where('dia', '>=', $mesRereInicio)
            ->sum('despesa');
        $lucroMesRerePassado = $valorMesRerePassado-$despesaMesRerePassado;

        $lava = new Lavacharts; // See note below for Laravel

        $finances = $lava->DataTable();

        $finances->addDateColumn('Year')
            ->addNumberColumn('Valor')
            ->setDateTimeFormat('m')
            ->addRow([$soRere, $lucroMesRerePassado])
            ->addRow([$soMesAnte, $lucroMesAntePassado])
            ->addRow([$soMesPassado, $lucroMesPassado])
            ->addRow([$soMesAtual, $lucroMesAtual]);

        $lava->ColumnChart('Finances', $finances, [
            'title' => 'Ultimos Lucros',
            'titleTextStyle' => [
                'color'    => '#eb6b2c',
                'fontSize' => 14
            ]
        ]);

        $solicitacaoServicos = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->where('estado', '=', 'concluido')
            ->orderBy('dia', 'desc')
            ->get();

        $valorLucroComSistema = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->sum('valor');
        $replaceValorLucroComSistema = number_format($valorLucroComSistema, 2, ',', ' ');

        $pessoas = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->where('estado', '=', 'concluido')
            ->get();

        $pessoasAtendidas = count($pessoas);

        return view('/gerencionamento', compact('lava', 'replaceLucrosMesAtual', 'replaceDespesaMesAtual', 'replaceValorMesAtual', 'solicitacaoServicos', 'replaceValorLucroComSistema','pessoasAtendidas'));
    }

    public function gerarPdfServicos()
    {
        $userId = Auth::user()->id;

        $valorLucroComSistema = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->sum('valor');
        $valor = number_format($valorLucroComSistema, 2, ',', ' ');

        $despesaLucroComSistema = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->sum('despesa');
        $despesa = number_format($despesaLucroComSistema, 2, ',', ' ');

        $valorLucro = $valorLucroComSistema-$despesaLucroComSistema;

        $lucro = number_format($valorLucro, 2, ',', ' ');

        $solicitacaoServicos = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->where('estado', '=', 'concluido')
            ->orderBy('dia', 'desc')
            ->get();

      $pdf =  \PDF::loadView('/pdfServicos', compact('solicitacaoServicos', 'valor', 'despesa', 'lucro'));
            // Se quiser que fique no formato a4 retrato: ->setPaper('a4', 'landscape')
         return $pdf->stream();

       // return view('/pdfServicos', compact('solicitacaoServicos', 'valor', 'despesa', 'lucro'));
    }

}
