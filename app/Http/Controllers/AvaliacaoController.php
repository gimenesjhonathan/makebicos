<?php

namespace App\Http\Controllers;

use App\Avaliacao;
use App\Notifications\NotificacaoComentario;
use App\SolicitacaoServico;
use App\User;
use Illuminate\Http\Request;
use Auth;
class AvaliacaoController extends Controller
{
    public function postAvaliarServico(Request $request, $servicoId, $userId){
        $name = Auth::user()->name;
        $sobrenome = Auth::user()->sobrenome;
        $nomeCompleto = $name." ".$sobrenome;
        $cliente_id = Auth::user()->id;

        if($request->avaliacao == 0) {
            return redirect()->back()->with('message', 'Marque uma avaliação');
        }
        $avaliacaoClientes = User::find($cliente_id)->avaliacaoCliente;

        if(count($avaliacaoClientes) == 0){
            if($request->avaliacao){
                $avaliacao = new Avaliacao();
                $avaliacao->cliente = $nomeCompleto;
                $avaliacao->avaliacao = $request->avaliacao;
                $avaliacao->user_id = $userId;
                $avaliacao->cliente_id = $cliente_id;
                $avaliacao->solicitacao_servico_id = $servicoId;
                $avaliacao->save();
            }

            $mensagem = $nomeCompleto.' avalio sua prestação de serviço';
            $notificacao = array("usuario" => $mensagem, "url" => "/servicos-concluidos");
            User::find($userId)->notify(new NotificacaoComentario($notificacao));

            return redirect('/servicos-solicitados')->with('message', 'Serviço foi avaliado Obrigado!');
        }

        $avaliacoes = SolicitacaoServico::find($servicoId)->avaliacao;
        if(count($avaliacoes) != 0){
            $avaliacao = $avaliacoes->first()->avaliacao;

            if($avaliacao != 0){
                return redirect('/servicos-solicitados')->with('message', 'Esse serviço já foi avaliado');
            }
        }
        else{
                if($request->avaliacao){
                    $avaliacao = new Avaliacao();
                    $avaliacao->cliente = $nomeCompleto;
                    $avaliacao->avaliacao = $request->avaliacao;
                    $avaliacao->user_id = $userId;
                    $avaliacao->cliente_id = $cliente_id;
                    $avaliacao->solicitacao_servico_id = $servicoId;
                    $avaliacao->save();
                }

            $notificacao = $nomeCompleto.' avalio sua prestação de serviço';

            User::find($userId)->notify(new NotificacaoComentario($notificacao));

            return redirect('/servicos-solicitados')->with('message', 'Serviço foi avaliado Obrigado!');
            }

    }

    public function getAvaliacao($id){
        $solicitacaoServicoClientes = SolicitacaoServico::find($id);
        $user = $solicitacaoServicoClientes->user;
        $userId = $user->id;
        $empresas = User::find($userId)->empresa;
        return view('/avaliar', compact('solicitacaoServicoClientes', 'empresas'));
    }

}
