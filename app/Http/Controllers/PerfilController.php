<?php

namespace App\Http\Controllers;

use App\Comentario;
use App\Perfil;
use App\User;
use App\Servico;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PerfilController extends Controller
{

    public function getPerfil($id){

        $user = User::find($id);
        $telefone = User::find($id)->servico;
        $endereco = User::find($id)->endereco;
        $servico = User::find($id)->servico;
        $perfil = User::find($id)->perfil;
        $comentarios = DB::table('comentarios')
            ->where('user_id', '=', $id)
            ->orderBy('id', 'desc')
            ->paginate(8);

        $avaliacoes = User::find($id)->avaliacao;
        $soma = $avaliacoes->sum('avaliacao');
        $quantidade = $avaliacoes->count('avaliacao');

        if(count($avaliacoes) != 0){
            $media = $soma/$quantidade;
        }else{ $media = ' ';}

        $userId = $id;
        if(count($servico) != 0){
            $idServico = $servico->first()->id;

            $categoria = Servico::find($idServico)->categoria;
        }
        return view('perfil', compact('userId', 'telefone','endereco','servico','perfil','categoria','user','comentarios', 'media'));
    }

 
}
