<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Endereco;
use App\ListCategoria;
use App\Notifications\NotificacaoComentario;
use App\Servico;
use App\SolicitacaoServico;
use App\Telefone;
use App\Perfil;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Categoria;
use Illuminate\Support\Facades\DB;
use Storage;


class UserController extends Controller
{
    private $endereco;
    private $telefone;
    private $servico;
    private $user;
    private $perfil;

    public function __construct(User $user, Endereco $endereco, Telefone $telefone, Servico $servico, Perfil $perfil)
    {
        $this->user = $user;
        $this->endereco = $endereco;
        $this->telefone = $telefone;
        $this->servico = $servico;
        $this->perfil = $perfil;
    }

    public function getListar()
    {
        $userId = Auth::User()->id;

        if (count(User::find($userId)->endereco) == 0){
            return redirect('/concluir');
        }

        $qtSolicitacaoNovas = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->where('estado', '=', 'novo')
            ->count();

        $qtSolicitacaoHoje = DB::table('solicitacao_servicos')
            ->where('user_id', '=', $userId)
            ->where('estado', '=', 'marcado')
            ->where('dia', '=', date('Y-m-d'))
            ->count();

        return view('/home', compact('qtSolicitacaoNovas', 'qtSolicitacaoHoje', 'userId'));
    }

    public function getUser($id)
    {

        $user = User::find($id);
        $userId = $user->id;
        $telefone = $user->telefone;
        $endereco = $user->endereco;

        return view("/concluir-alterar", compact('user', 'telefone', 'endereco', 'userId'));
    }

    public function putAlterar(Request $request, $id)
    {
        $userId = Auth::user()->id;

        //update do User
        if ($request->name && $request->sobrenome) {
            $this->validate($request, $this->telefone->rules);
            $user = User::find($id);
            $user->name = $request->name;
            $user->sobrenome = $request->sobrenome;
            $user->save();
            return redirect("/concluir-alterar/{$userId}/edit")->with('message', 'Nome do usuário Alterado');
        }

        //update do Telefone
        if ($request->fone) {
            $this->validate($request, $this->telefone->rules);
            $telefone = Telefone::find($id);
            $telefone->fone = $request->fone;
            $telefone->save();
            return redirect("/concluir-alterar/{$userId}/edit")->with('message', 'Telefone Alterado');
        }
        //update do Endereco
        if ($request->estado && $request->cidade && $request->bairro && $request->logradouro && $request->numero) {
            $this->validate($request, $this->endereco->rules);
            $endereco = Endereco::find($id);
            $endereco->estado = $request->estado;
            $endereco->cidade = $request->cidade;
            $endereco->bairro = $request->bairro;
            $endereco->logradouro = $request->logradouro;
            $endereco->numero = $request->numero;
            $endereco->save();
            return redirect("/concluir-alterar/{$userId}/edit")->with('message', 'Endereco Alterado');
        }

        return redirect("/concluir-alterar/{$userId}/edit")->with('message', 'Verifique os campos preenchidos');
    }

    public function messages()
    {
        return [
            'fone.required' => 'Campo Telefone é Obrigatório',
            'fone.min' => 'Telefone inválido',
            'fone.max' => 'Telefone inválido',

            'estado.required' => 'Campo Estado é Obrigatório',
            'estado.max' => 'Estado inválido',

            'cidade.required' => 'Campo Cidade é Obrigatório',
            'cidade.max' => 'Cidade inválido',

            'bairro.max' => 'Bairro inválido',

            'logradouro.max' => 'Logradouro inválido',

            'numero.max' => 'Logradouro inválido',
            'numero.required' => 'Campo Número é Obrigatório',

            'servico.required' => 'Campo Serviço é Obrigatório',
            'servico.max' => 'Serviço inválido',

            'desc_servico.required' => 'Campo Descrição do seu Serviço é Obrigatório',
            'desc_servico.min' => 'Campo descrição do seu Serviço o mínimo 20 dígitos',
            'desc_servico.max' => 'Campo descrição do seu Serviço o máximo 250 dígitos',

            'categoria_id.required' => 'Campo Categoria é Obrigatório',

            'image.required' => 'Escolha uma imagem',
            'image.image' => 'Escolha imagens com extensão .JPG, .PNG e .JFIF'
        ];
    }

    public function postTelefoneEndereco(Request $request)
    {
        $currentuserid = Auth::user()->id;


        $validate = validator($request->all(), $this->telefone->rules, $this->messages());

        if ($validate->fails()) {
            return redirect('concluir')
                ->withErrors($validate)
                ->withInput();
        }

        $validate = validator($request->all(), $this->endereco->rules, $this->messages());

        if ($validate->fails()) {
            return redirect('concluir')
                ->withErrors($validate)
                ->withInput();
        }

        if ($request->fone) {
            $telefone = new Telefone();
            $telefone->fone = $request->fone;
            $telefone->user_id = $currentuserid;
            $telefone->save();

        }

        if ($request->fone2) {
            $telefone = new Telefone();
            $telefone->fone = $request->fone2;
            $telefone->user_id = $currentuserid;
            $telefone->save();
        }

        if ($request->fone3) {
            $telefone = new Telefone();
            $telefone->fone = $request->fone3;
            $telefone->user_id = $currentuserid;
            $telefone->save();
        }

        if ($request->estado && $request->cidade || $request->bairro || $request->logradouro || $request->numero) {
            $endereco = new Endereco();
            $endereco->estado = $request->estado;
            $endereco->cidade = $request->cidade;
            $endereco->bairro = $request->bairro;
            $endereco->logradouro = $request->logradouro;
            $endereco->numero = $request->numero;
            $endereco->user_id = $currentuserid;
            $endereco->save();

            $perfil = new Perfil();
            $perfil->image = '/img/fotos/vazio.jpg';
            $perfil->user_id = $currentuserid;
            $perfil->save();
        }

        return redirect("/escolher");
    }

    public function postServicoEmpresa(Request $request)
    {

        $currentuserid = Auth::user()->id;
        $validate = validator($request->all(), $this->servico->rules, $this->messages());

        if ($validate->fails()) {
            return redirect()->back()
                ->withErrors($validate)
                ->withInput();
        }

        if ($request->servico && $request->desc_servico && $request->categoria_id) {
            $servico = new Servico();
            $servico->servico = $request->servico;
            $servico->desc_servico = $request->desc_servico;
            $servico->categoria_id = $request->categoria_id;
            $servico->user_id = $currentuserid;
            $servico->save();
        }

        if ($request->nome_empresa) {
            $empresa = new Empresa();
            $empresa->nome_empresa = $request->nome_empresa;
            $empresa->user_id = $currentuserid;
            $empresa->save();
        }

        return redirect("/perfil")->with("message", "Usuário cadastrado");
    }

    public function postAvatar(Request $request, $id)
    {

        $validate = validator($request->all(), $this->perfil->rules, $this->messages());

        if ($validate->fails()) {
            return redirect()->back()
                ->withErrors($validate)
                ->withInput();
        }

        $nameFile = null;

        // Verifica se informou o arquivo e se é válido
        if ($request->image) {

            // Define um aleatório para o arquivo baseado no timestamps atual
            $name = uniqid(date('HisYmd'));

            // Recupera a extensão do arquivo
            $extension = $request->image->extension();

            // Define finalmente o nome
            $nameFile = "{$name}.{$extension}";

            // Faz o upload:

            //$upload = $request->image->storeAs('users', $nameFile);

            $upload = $request->file('image')->store('avatars');

            $userPerfil = User::find($id)->perfil;

            $idPerfil = $userPerfil->first()->id;

            $perfil = Perfil::find($idPerfil);

            $perfil->image = $upload;

            $perfil->save();

            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if (!$upload)
                return redirect()
                    ->back()
                    ->with('error', 'Falha ao fazer upload')
                    ->withInput();

        }
        return redirect("/concluir-alterar/{$id}/edit")->with('message', 'Foto Alterada');
    }

    public function getConfirmarConta($id){

        $user = User::find($id);
        $user->status = 'true';
        $user->save();

        return redirect('/concluir')->with('message', 'Conta confirmada');
    }

    public function getConcluir(){
        $userId = Auth::User()->id;

        if (User::find($userId)->status == 'false'){
            $email = User::find($userId)->email;

            $mensagem = 'Confirmar sua conta';
            $notificacao = array("usuario" => $mensagem, "url" => "/confirmar-conta/{$userId}");
            User::find($userId)->notify(new NotificacaoComentario($notificacao));

            return view('/verifique', compact('email'));
        }else{
            return view('concluir');
        }
    }

}
