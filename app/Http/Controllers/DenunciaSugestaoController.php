<?php

namespace App\Http\Controllers;

use App\DenunciaSugestao;
use App\User;
use Illuminate\Http\Request;
use Auth;

class DenunciaSugestaoController extends Controller
{
    private $denunciaSugestao;

    public function __construct(DenunciaSugestao $denunciaSugestao)
    {
        $this->denunciaSugestao = $denunciaSugestao;
    }

    public function messages(){
        return[
            'mensagem.required' => 'Campo de descrição da Sugestão ou Denuncia é obrigatório',
            'mensagem.max' => 'O maximo de caracteres são 250'
        ];
    }

    public function postCadastrarDenunciaSugestao(Request $request)
    {
        $userId = Auth::user()->id;
        $name = Auth::user()->name;
        $sobrenome = Auth::user()->sobrenome;
        $nomeCompleto = $name." ".$sobrenome;

        $validate = validator($request->all(), $this->denunciaSugestao->rules, $this->messages());

        if($validate->fails()){
            return redirect()->back()
                ->withErrors($validate)
                ->withInput();
        }

        /*
        if($request->mensagem == null){
            return redirect()->back()->with('message', 'Preenche o campo de Sugestão ou Denuncia');
        }
        */
        if($request->mensagem){
            $denunciaSugestao = new DenunciaSugestao();
            $denunciaSugestao->mensagem = $request->mensagem;
            $denunciaSugestao->usuario = $nomeCompleto;
            $denunciaSugestao->user_id = $userId;
            $denunciaSugestao->save();
        }
        return redirect('/home')->with('message', 'Sua denuncia ou sugestão foi enviada');
    }
}
