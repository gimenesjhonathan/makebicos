<?php

namespace App\Http\Controllers;

use App\Galeria;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use Validator;


class GaleriaController extends Controller
{
    private $galeria;

    public function __construct(Galeria $galeria)
    {
        $this->galeria = $galeria;
    }

    public function messages(){
        return[
            'foto.required' => 'Você não escolheu as fotos',
            'foto.*' => 'Você não selecionou uma imagem'
        ];
    }

    public function getGaleria($id){

        //$galerias = User::find($id)->galeria;
        $galerias = DB::table('galerias')
            ->where('user_id', '=', $id)
            ->paginate(15);

        return view('/galeria', compact('galerias', 'id'));
    }

    public function postAdicionarFoto(Request $request)
    {
        $userId = Auth::user()->id;

        /*
        $validate = Validator::make($request->all(), [
            'foto.*' => 'required | image']
        );*/

        $validate = validator($request->all(), $this->galeria->rules, $this->messages());

        if($validate->fails()){
            return redirect()->back()
                ->withErrors($validate)
                ->withInput();
        }

        if ($request->hasfile('foto')){

            foreach ($request->file('foto') as $foto) {

                $nameFile = null;

                // Define um aleatório para o arquivo baseado no timestamps atual
                $name = uniqid(date('HisYmd'));

                // Recupera a extensão do arquivo
                $extension = $foto->extension();

                // Define finalmente o nome
                $nameFile = "{$name}.{$extension}";

                // Faz o upload:
                $upload = $foto->storeAs('galeria', $nameFile);
                // Se tiver funcionado o arquivo foi armazenado em storage/app/public/galeria/nomedinamicoarquivo.extensao

                $data[] = $upload;

            }
        }
        foreach ($data as $nova){
            $galeria = new Galeria();
            $galeria->foto = $nova;
            $galeria->user_id = $userId;
            $galeria->save();
        }

        return redirect()->back()->with('message', 'Fotos Cadastradas');
    }

    public function postAlterarNomeFoto(Request $request, $id){

        if($request->nome_foto == null){
            return redirect()->back()->with('message', 'Nome da foto estava Vazio');
        }

        if($request->nome_foto){
            $galeria = Galeria::find($id);
            $galeria->nome_foto = $request->nome_foto;
            $galeria->save();
        }
        return redirect()->back()->with('message', 'Nome da foto Foi alterada');
    }
}