<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\ListCategoria;
use App\User;
use Auth;
use App\Servico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   // public function index()
   // {
   //     return view('home');
   // }

    public function index2(){

        $userId = Auth::user()->id;
        $user = User::find($userId);

        $telefone = User::find($userId)->servico;
        $endereco = User::find($userId)->endereco;
        $servico = User::find($userId)->servico;
        $perfil = User::find($userId)->perfil;
        $comentarios = DB::table('comentarios')
            ->where('user_id', '=',$userId)
            ->orderBy('id', 'desc')
            ->paginate(8);

        $avaliacoes = User::find($userId)->avaliacao;
        $soma = $avaliacoes->sum('avaliacao');
        $quantidade = $avaliacoes->count('avaliacao');

        if(count($servico) != 0){
            $idServico = $servico->first()->id;

            $categoria = Servico::find($idServico)->categoria;
        }

        if(count($avaliacoes) != 0){
            $media = $soma/$quantidade;
        }else{ $media = ' ';}

        $categorias = Categoria::all();

        return view('perfil', compact('userId', 'telefone','endereco','servico','perfil','categoria','user', 'comentarios', 'media', 'categorias'));
    }



}
