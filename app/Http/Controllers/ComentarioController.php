<?php

namespace App\Http\Controllers;

use App\Comentario;
use App\Notifications\NotificacaoComentario;
use App\Resposta;
use App\Servico;
use App\User;
use Illuminate\Http\Request;
use Auth;
use App\Mail\Welcome;

class ComentarioController extends Controller
{
    public function postSalvarComentario(Request $request, $id){

        $clienteId = Auth::user()->id;
        $name = Auth::user()->name;
        $sobrenome = Auth::user()->sobrenome;
        $mensagem = $request->mensagem;
        $nomeCompleto = $name." ".$sobrenome;


        if($request->mensagem){
        $comentario = new Comentario();
        $comentario->cliente = $nomeCompleto;
        $comentario->mensagem = $request->mensagem;
        $comentario->user_id = $id;
        $comentario->cliente_id = $clienteId;
        $comentario->save();
        }

        $mensagem = $nomeCompleto.' fez um comentário';
        $notificacao = array("usuario" => $mensagem, "url" => "/perfil");
        User::find($id)->notify(new NotificacaoComentario($notificacao));

        return redirect()->back()->with('message', 'Comentario salvo');
    }

    public function postSalvarResposta(Request $request, $id, $idComentario){

        $clienteId = Auth::user()->id;
        $name = Auth::user()->name;
        $sobrenome = Auth::user()->sobrenome;

        $nomeCompleto = $name." ".$sobrenome;

        if($request->resposta){
            $resposta = new Resposta();
            $resposta->usuario = $nomeCompleto;
            $resposta->resposta = $request->resposta;
            $resposta->comentario_id = $idComentario;
            $resposta->user_id = $clienteId;
            $resposta->save();
        }

        $user = User::find($id);
        $telefone = User::find($id)->servico;
        $endereco = User::find($id)->endereco;
        $servico = User::find($id)->servico;
        $perfil = User::find($id)->perfil;
        $comentarios = User::find($id)->comentario;
        $respostas = Comentario::find($id)->resposta;

        if(count($servico) != 0){
            $idServico = $servico->first()->id;

            $categoria = Servico::find($idServico)->categoria;
        }
        $userId = $id;

        $avaliacoes = User::find($id)->avaliacao;
        $soma = $avaliacoes->sum('avaliacao');
        $quantidade = $avaliacoes->count('avaliacao');

        if(count($avaliacoes) != 0){
            $media = $soma/$quantidade;
        }else{ $media = ' ';}

        return view("/perfil", compact('user', 'telefone', 'endereco', 'servico', 'perfil', 'userId', 'categoria', 'comentarios','respostas', 'media'));
    }
}
