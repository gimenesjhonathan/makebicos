<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $fillable = [
        'id',
        'estado',
        'cidade',
        'bairro',
        'logradouro',
        'numero',
        'user_id'
    ];

    protected $table = 'enderecos';

    public $rules = [
        'estado' => 'required | max:100',
        'cidade' => 'required | max:100',
        'bairro' => 'required |max:100',
        'logradouro' => 'required | max:100',
        'numero' => 'required | max:7'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
