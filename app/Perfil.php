<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $fillable = [
        'id',
        'image',
        'user_id'
    ];

    public $rules = [
        'image' => 'required | image'
    ];

    protected $table = 'perfis';

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
