<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    protected $fillable = [
        'id',
        'servico',
        'desc_servico',
        'user_id',
        'categoria_id',

    ];

    public $rules = [
        'servico' => 'required|max:100',
        'desc_servico' => 'required|min:20|max:250',
        'categoria_id' => 'required'
    ];

    protected $table = 'servicos';

    public function categoria(){
        return $this->belongsTo(Categoria::class, 'categoria_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

}
