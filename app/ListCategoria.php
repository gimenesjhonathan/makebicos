<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListCategoria extends Model
{
    protected $fillable = [
        'id',
        'servico',
        'categoria_id'
    ];

    protected $table = 'list_categorias';

    public static function listServicos($id){
        return ListCategoria::where('categoria_id', '=', $id)
        ->get();
    }

    public function categoria(){
        return $this->belongsTo(Categoria::class, 'categoria_id');
    }
}
