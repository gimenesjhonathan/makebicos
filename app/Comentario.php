<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $fillable = [
        'id',
        'cliente',
        'mensagem',
        'user_id',
        'cliente_id'
    ];

    protected $table = 'comentarios';

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function cliente(){
        return $this->belongsTo(User::class, 'cliente_id');
    }

    public function resposta(){
        return $this->hasMany(Resposta::class, 'comentario_id');
    }
}
