<?php

namespace App;

use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'sobrenome', 'email', 'password', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $rules = [
        'name' => 'required|string|max:255',
        'sobrenome' => 'required|string|max:255'
    ];


    public function telefone(){
        return $this->hasMany(Telefone::class, 'user_id');
    }

    public function endereco(){
        return $this->hasMany(Endereco::class, 'user_id');
    }

    public function servico(){
        return $this->hasMany(Servico::class, 'user_id');
    }

    public function perfil(){
        return $this->hasMany(Perfil::class, 'user_id');
    }

    public function solicitacaoServico(){
        return $this->hasMany(SolicitacaoServico::class, 'user_id');
    }

    public function solicitacaoServicoCliente(){
        return $this->hasMany(SolicitacaoServico::class, 'cliente_id');
    }

    public function denunciaSugestao(){
        return $this->hasMany(DenunciaSugestao::class, 'user_id');
    }

    public function avaliacao(){
        return $this->hasMany(Avaliacao::class, 'user_id');
    }

    public function avaliacaoCliente(){
        return $this->hasMany(Avaliacao::class, 'cliente_id');
    }

    public function comentario(){
        return $this->hasMany(Comentario::class, 'user_id');
    }

    public function comentarioCliente(){
        return $this->hasMany(Comentario::class, 'cliente_id');
    }

    public function resposta(){
        return $this->hasMany(Resposta::class, 'user_id');
    }

    public function empresa(){
        return $this->hasMany(Empresa::class, 'user_id');
    }

    public function galeria(){
        return $this->hasMany(Galeria::class, 'user_id');
    }

}
