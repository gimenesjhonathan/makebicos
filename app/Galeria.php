<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galeria extends Model
{
    protected $fillable = [
        'id',
        'foto_nome',
        'foto',
        'user_id'
    ];

    protected $table = 'galerias';

    public $rules = [
        'foto' => 'required',
        'foto.*' =>  'image'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
