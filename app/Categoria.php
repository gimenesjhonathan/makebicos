<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $fillable = [
        'id',
        'categoria'
    ];

    protected $table = 'categorias';

    public function servico(){
        return $this->hasMany(Servico::class, 'categoria_id');
    }

    public function listCategoria(){
        return $this->hasMany(ListCategoria::class, 'categoria_id');

    }
}
