<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::middleware(['auth'])->group(function (){
    Route::get('/categorias', function (){
        $categorias = \App\Categoria::all();
        $servicos = " ";
        $id = " ";
        return view('/categorias', compact('categorias', 'servicos', 'id'));
    });
    Route::post("/cadastrar-categoria", "CategoriaController@postCadastrarCategoria");
    Route::post("/cadastrar-servico", "CategoriaController@postCadastrarServico");

    Route::get('/home', 'UserController@getListar');

    Route::get('/perfil', 'HomeController@index2')->name('home');
    Route::get("/perfil/{idUser}/index3", "PerfilController@getPerfil");
    Route::post("/store", "UserController@postTelefoneEndereco");
    Route::post("/stores", "UserController@postServicoEmpresa");
    Route::post("/avatar/{userId}", "UserController@postAvatar");
    Route::put("/concluir-alterar/update/{userId}", "UserController@putAlterar");
    Route::get("/concluir-alterar/{userId}/edit", "UserController@getUser");
    Route::post("/adicionar-telefone", "TelefoneController@postAdicionaTelefone");
    Route::post("/adicionar-endereco", "EnderecoController@postAdicionaEndereco");
    Route::post("/adicionar-foto", "GaleriaController@postAdicionarFoto");

    Route::get("/concluir", "UserController@getConcluir");
    Route::get("/concluirs", "CategoriaController@getCategorias");
    Route::get("/concluirs/categoria/{id}", "CategoriaController@getServicos");

    Route::get("/galeria/{id}", "GaleriaController@getGaleria");
    Route::post("/adicionar-foto/{id}", "GaleriaController@postAlterarNomeFoto");

    Route::delete("/deletar-servico/{id}", "ServicoController@deleteServico");
    Route::put("/alterar-servico/{id}", "ServicoController@putAlterarServico");
    Route::get("/solicitar/{userId}", "SolicitarServicoController@getSolicitar");
    Route::post("/solicitar/{id}/cadastrarsolicitacao", "SolicitarServicoController@postCadastrarSolicitacao");
    Route::get("/servicos", "SolicitarServicoController@getServicos");
    Route::get("/servicos-marcados", "SolicitarServicoController@getServicosMarcados");
    Route::get("/servicos-concluidos", "SolicitarServicoController@getServicosConcluidos");
    Route::get("/servicos-vencidos", "SolicitarServicoController@getServicosVencidos");
    Route::get("/servicos-descartados", "SolicitarServicoController@getServicosDescartados");
    Route::get("/servicos-solicitados", "SolicitarServicoController@getServicosSolicitados");
    Route::get("/servicos-solicitados-concluido", "SolicitarServicoController@listaServicosSolicitadosConcluidos");
    Route::get("/servicos-solicitados-descartado", "SolicitarServicoController@listaServicosSolicitadosDescartados");
    Route::get("/servicos-solicitados-marcado", "SolicitarServicoController@listaServicosSolicitadosMarcados");
    Route::get("/servicos-solicitados-nao-atendidos", "SolicitarServicoController@listaServicosSolicitadosNovos");
    Route::get("/servicos-solicitados-vencidos", "SolicitarServicoController@listaServicosSolicitadosVencidos");
    Route::get("/servicos-marcado", function (){
        return view('/servicos-marcado');
    });
    //Route::get("/servicos-descartado", "SolicitarServicoController@listaServicosDescartados");
    //Route::get("/servicos-concluido", "SolicitarServicoController@listaServicosConcluidos");
    //Route::get("/servicos-nao-atendidos", "SolicitarServicoController@listaServicosNaoAtendidos");
    //Route::get("/servicos-vencidos", "SolicitarServicoController@listaServicosVencidos");
    Route::put("/descartar-servico/{id}", "SolicitarServicoController@putDescartarServico");
    Route::put("/concluir-servico/{id}", "SolicitarServicoController@putConcluirServico");
    Route::put("/atender-servico/{id}", "SolicitarServicoController@putCadastrarHorarioAtendimento");
    Route::delete("/excluir-servico/{id}", "SolicitarServicoController@deleteServico");
    Route::post("/comentar/{userId}", "ComentarioController@postSalvarComentario");
    Route::post("/responder/{id}/{idComentario}", "ComentarioController@postSalvarResposta");
    Route::post("/avaliar/{servicoId}/{userId}", "AvaliacaoController@postAvaliarServico");
    Route::post("/cadastrar-denuncias-sugestoes", "DenunciaSugestaoController@postCadastrarDenunciaSugestao");
    Route::get('/denuncias-sugestoes', function () {
        return view('denuncias-sugestoes');
    });
    Route::get('/escolher', function () {
        return view('/escolher');
    });
    Route::get("/avaliar/{id}", "AvaliacaoController@getAvaliacao");
    Route::get("/gerencionamento", "SolicitarServicoController@grafico");

    Route::get('/pdf', 'SolicitarServicoController@gerarPdfServicos')->name('pdf');
});

Route::get("/buscar", "CategoriaController@getBuscar");
Route::get("/buscar/categoria/{id}", "CategoriaController@getBuscarServico");
Route::post("/pesquisar", "CategoriaController@postPesquisar");
Route::get("/confirmar-conta/{id}", "UserController@getConfirmarConta");
