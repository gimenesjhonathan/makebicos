@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row mt-4">
            @foreach($users as $user)
                <div class="col-md-3 mb-4 border-0 p-2">
                    <a href="{{url("/perfil/{$user->id}/index3")}}">
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @php
                                    $i = '1';
                                @endphp
                                @if(count($user->galeria) == 0)
                                    <div class="carousel-item @if($i == 1) active @else @endif">
                                        <img class="d-block w-100 border border-white"
                                             src="{{url('/img/fotos/sem-foto-na-galeria.jfif')}}"
                                             style="height: 10rem;" alt="First slide">
                                    </div>
                                @endif
                                @foreach($user->galeria as $galeria)
                                    <div class="carousel-item @if($i == 1) active @else @endif">
                                        <img class="d-block w-100 border border-white"
                                             src="{{ asset($galeria->foto)}}"
                                             style="height: 10rem;" alt="First slide">
                                    </div>
                                    @php
                                        $i++;
                                    @endphp
                                @endforeach
                            </div>
                        </div>
                        <div class="card border-0 shadow rounded-0" style="height: 8rem;">
                            @foreach($user->perfil as $perfil)
                                <img class="card-img-top perfil rounded-circle shadow ml-1"
                                     src="{{ asset($perfil->image) }}"
                                     style="width: 4rem; height: 4rem; margin-top: -2rem;">
                                @foreach($user->empresa as $empresa)
                                    <p class="font-weight-bold w-100 pl-2 mb-0">{{$empresa->nome_empresa}}</p>
                                @endforeach
                            @endforeach
                            @php
                                $e = 1;
                            @endphp
                            @foreach($user->endereco as $endereco)
                                @if($e == 1)
                                    <p class="ml-2 mb-0">{{$endereco->cidade}} - {{$endereco->estado}}</p>
                                @endif
                                @php
                                    $e++;
                                @endphp
                            @endforeach
                            @php
                                $avaliacoes = $user->avaliacao;
                                $soma = $avaliacoes->sum('avaliacao');
                                $quantidade = $avaliacoes->count('avaliacao');

                                if(count($avaliacoes) != 0){
                                    $media = $soma/$quantidade;
                                }else{ $media = ' ';}
                            @endphp
                            <div class="mt-0 mb-2 ml-1">
                                @if($media >= 4.9)
                                    <img src="{{url('/img/avaliacao/avaliacao5.png')}}"
                                         style="height: 1rem; width: 5rem;">
                                @endif
                                @if($media >= 4.5 && $media <= 4.8)
                                    <img src="{{url('/img/avaliacao/avaliacao4.5.png')}}"
                                         style="height: 1rem; width: 5rem;">
                                @endif
                                @if($media >= 4 && $media <= 4.4)
                                    <img src="{{url('/img/avaliacao/avaliacao4.png')}}"
                                         style="height: 1rem; width: 5rem;">
                                @endif
                                @if($media >= 3.5 && $media <= 3.9)
                                    <img src="{{url('/img/avaliacao/avaliacao3.5.png')}}"
                                         style="height: 1rem; width: 5rem;">
                                @endif
                                @if($media >= 3 && $media <= 3.4)
                                    <img src="{{url('/img/avaliacao/avaliacao3.png')}}"
                                         style="height: 1rem; width: 5rem;">
                                @endif
                                @if($media >= 2.5 && $media <= 2.9)
                                    <img src="{{url('/img/avaliacao/avaliacao2.5.png')}}"
                                         style="height: 1rem; width: 5rem;">
                                @endif
                                @if($media >= 2 && $media <= 2.4)
                                    <img src="{{url('/img/avaliacao/avaliacao2.png')}}"
                                         style="height: 1rem; width: 5rem;">
                                @endif
                                @if($media >= 1.5 && $media <= 1.9)
                                    <img src="{{url('/img/avaliacao/avaliacao1.5.png')}}"
                                         style="height: 1rem; width: 5rem;">
                                @endif
                                @if($media >= 1 && $media <= 1.4)
                                    <img src="{{url('/img/avaliacao/avaliacao1.png')}}"
                                         style="height: 1rem; width: 5rem;">
                                @endif
                                @if($media >= 0.5 && $media <= 0.9)
                                    <img src="{{url('/img/avaliacao/avaliacao0.5.png')}}"
                                         style="height: 1rem; width: 5rem;">
                                @endif
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection