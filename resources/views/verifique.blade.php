<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="bg-light">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MakeBicos') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/fontawesome-free-5.3.1-web/css/all.min.css">
    <link rel="stylesheet" href="/bulma-0.7.1/css/bulma.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <style>
        body {
            margin: 0px;
            padding: 0px;
        }

        .perfil {
            width: 7rem;
            height: 7rem;
        }

        .capa {
            width: 100%;
            height: 10rem;
        }

        .card-footer {
            background: #EC7063 !important;

        }

        .card-footer .text-muted {
            color: #fff !important;
        }

        a:link {
            color: black !important;
            text-decoration: none
        }

        /* link que foi visitado */
        a:visited {
            color: black;
        !important;
        }

        p {
            font-size: 1rem;
        }


        .estrelas input[type=radio]{
            display: none;
        }.estrelas label i.fa:before{
             content: '\f005';
             color: #FC0;
         }.estrelas  input[type=radio]:checked  ~ label i.fa:before{
              color: #CCC;
          }


    </style>
</head>
<body class="bg-light">
    <div class="container">
        <h1 class="mt-5 text-center">Verifique seu Email para confirmar sua conta </h1>
        <h3 class="mt-2 text-center">{{$email}}</h3>
        <h4 class="mt-5 text-center">MakeBicos Agradece!</h4>

        <div class="text-center mt-5 "><h4>Caso email está incorreto</h4></div>
        <div class="text-center"><a class="btn btn-primary" href="{{ route('logout') }}" style="color: #FFFFFF!important;"
                                    onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                Email incorreto
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </div>
</body>
</html>
