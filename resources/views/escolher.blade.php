@extends('layouts.app')
@section('content')
    <div class="container mt-5">
        <div class="card  rounded-0 border-0 shadow">
            <div class="card-body pt-5">
                <div class="text-center">
                <h3>O que você deseja?</h3>
                <a href="{{url ('/buscar') }}" class="btn btn-primary" style="color: #ffffff;">Buscar Serviço</a>
                <a href="{{ url ('/concluirs') }}" class="btn btn-danger" style="color: #ffffff;">Publicar Empresa</a>
                </div>
            </div>
        </div>
    </div>
@endsection