@extends('layouts.app')
@section('content')
    <form action="{{url("/cadastrar-denuncias-sugestoes")}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="container">
            @if(\Session::has('message'))
                <div class="alert alert-danger rounded-0 border-0 shadow mt-4" role="alert">
                    <ul>
                        <li>{!! \Session::get('message') !!}</li>
                    </ul>
                </div>
            @endif
            @if (isset($errors) && count($errors) > 0)
                <div class="alert alert-danger rounded-0 border-0 shadow mt-2 m-0 border-0" role="alert">
                    @foreach($errors->all() as $erro)
                        <p>{{$erro}}</p>
                    @endforeach
                </div>
            @endif
            <div class="card  rounded-0 border-0 shadow mt-4">
                <div class="card-header">{{ __('Denuncia e Sugestões') }}</div>
                <div class="card-body">
                    <div class="form-group">
                        <textarea class="form-control" id="mensagem" name="mensagem" placeholder="Registre uma Denuncia ou Sugestão..." rows="3"></textarea>
                    </div>
                    <div class="text-right">
                        <button href="{{redirect()->back()}}" class="btn btn-danger rounded-0 border-0" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success rounded-0">Salvar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection