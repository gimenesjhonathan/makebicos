@extends('layouts.app')
@section('content')
    <form action="{{url("/stores")}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="container">
            <div class="card  rounded-0 border-0 shadow mt-4">
                <div class="card-body">
                    <h3 class="border-bottom p-1">Dados Empresa</h3>
                    <div class="row">
                        <div class="col-md-6 mb-2 mt-2">
                            <select id="categoria_id" name="categoria_id" onchange="categoria()"
                                    class="form-control{{ $errors->has('categoria_id') ? ' is-invalid' : '' }}">
                                <option value="">Selecione Categoria</option>
                                @foreach($categorias as $categoria)
                                    @if($categoria->id == old('categoria_id'))
                                        <option value="{{$categoria->id}}" selected>{{$categoria->categoria}}</option>
                                    @endif
                                    @if($id == " ")
                                        <option value="{{$categoria->id}}">{{$categoria->categoria}}</option>
                                    @else
                                        @if($id == $categoria->id))
                                        <option value="{{$categoria->id}}" selected>{{$categoria->categoria}}</option>
                                        @else
                                            <option value="{{$categoria->id}}">{{$categoria->categoria}}</option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>
                            @if ($errors->has('categoria_id'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('categoria_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-md-6 mb-2 mt-2">
                            <select id="servico" name="servico"
                                    class="form-control{{ $errors->has('servico') ? ' is-invalid' : '' }}">
                                @if($servicos == " ")
                                    <option value="">Selecione Servico</option>
                                @else
                                    @foreach($servicos as $servico)
                                        @if($servico->servico == old('servico'))
                                            <option value="{{$servico->servico}}" selected>{{$servico->servico}}</option>
                                        @endif
                                        <option value="{{$servico->servico}}">{{$servico->servico}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        @php
                            $id = Auth::user()->id;
                            $empresas = \App\User::find($id)->empresa;
                        @endphp
                        @if(count($empresas) == 0)
                        <div class="col-md-12">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input id="nome_empresa" type="text"
                                           class="form-control{{ $errors->has('nome_empresa') ? ' is-invalid' : '' }}"
                                           placeholder="Nome da Empresa" name="nome_empresa"
                                           value="{{ old('nome_empresa') }}" required autofocus>
                                    @if ($errors->has('nome_empresa'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nome_empresa') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @else
                        @endif
                        <div class="col-md-12 mb-2">
                            <textarea class="form-control{{ $errors->has('desc_servico') ? ' is-invalid' : '' }}"
                                      name="desc_servico" rows="6"
                                      id="comment"
                                      placeholder="Descreva sobre seu Serviço...">{{old('desc_servico')}}</textarea>
                            @if ($errors->has('desc_servico'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('desc_servico') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-md-12">
                            <div class="text-right">
                                <button type="submit" class="btn btn-success rounded-0 border-0 mb-2">Salvar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        function categoria() {
            window.location = ("/concluirs/categoria/" + document.getElementById("categoria_id").value);
        }
    </script>
@endsection