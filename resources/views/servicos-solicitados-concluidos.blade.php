@extends('layouts.app')
@section('content')
    <div class="container">
        @if(\Session::has('message'))
            <div class="alert alert-danger rounded-0 border-0 shadow mt-4" role="alert">
                <ul>
                    <li>{!! \Session::get('message') !!}</li>
                </ul>
            </div>
        @endif
        <div class="card rounded-0 border-0 shadow">
            <div class="card-header">Serviços Solicitados</div>
            <div class="card-body">
                <label for="categoria_id">Filtrar</label>
                <select id="estado-servico" name="estado-servico" onchange="filtro()"
                        class="form-control mb-4 col-3{{ $errors->has('categoria_id') ? ' is-invalid' : '' }}">
                    <option value="">Selecione...</option>
                    <option value="concluido">Concluidos</option>
                    <option value="marcado">Com horário marcado</option>
                    <option value="descartado">Descartados</option>
                    <option value="nao-atendidos">Não Atendidos</option>
                    <option value="vencidos">Vencidos</option>
                </select>
                <div class="row">
                    @foreach($solicitacaoServicoClientes as $solicitacaoServicoCliente)
                        @php
                            $idSolicitacaoServicoCliente = $solicitacaoServicoCliente->id;
                            $avaliacoes = App\SolicitacaoServico::find($idSolicitacaoServicoCliente)->avaliacao;
                            $userIdSolicitacaoServicoCliente = $solicitacaoServicoCliente->user_id;
                            $empresas = App\User::find($userIdSolicitacaoServicoCliente)->empresa;

                        @endphp
                        <div class="col-md-12 mb-4">
                            <div class="card rounded-0 border-0 shadow">
                                <div class="card-body">
                                   <div class="row">
                                        <div class="col-md-8">
                                       <h4>
                                            @foreach($empresas as $empresa)
                                                {{$empresa->nome_empresa}}
                                                @endforeach
                                            </h4>
                                        </div>
                                        <div class="col-md-4">
                                            @if($solicitacaoServicoCliente->estado == 'concluido')
                                            <p class="text-right">Concluído</p>
                                            @endif
                                        </div>
                                    </div>
                                    <p>{{$solicitacaoServicoCliente->mensagem}}</p>
                                    @foreach($avaliacoes as $avaliacao)
                                        @if($avaliacao->avaliacao == 5)
                                            <img src="{{url('/img/avaliacao/avaliacao5.png')}}" style="height: 2rem; width: 10rem;">
                                        @endif
                                        @if($avaliacao->avaliacao == 4)
                                            <img src="{{url('/img/avaliacao/avaliacao4.png')}}" style="height: 2rem; width: 10rem;">
                                        @endif
                                        @if($avaliacao->avaliacao == 3)
                                            <img src="{{url('/img/avaliacao/avaliacao3.png')}}" style="height: 2rem; width: 10rem;">
                                        @endif
                                        @if($avaliacao->avaliacao == 2)
                                            <img src="{{url('/img/avaliacao/avaliacao2.png')}}" style="height: 2rem; width: 10rem;">
                                        @endif
                                        @if($avaliacao->avaliacao == 1)
                                            <img src="{{url('/img/avaliacao/avaliacao1.png')}}" style="height: 2rem; width: 10rem;">
                                        @endif
                                    <p class="card-text">Avaliação: {{$avaliacao->avaliacao}}</p>
                                    @endforeach
                                    @if(count($avaliacoes) == 0)
                                        <a href="{{url("/avaliar/{$solicitacaoServicoCliente->id}")}}">Avaliar</a>
                                    @else
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <script>
        function filtro() {
            window.location = ("/servicos-solicitados-"+ document.getElementById("estado-servico").value);
        }
    </script>
@endsection