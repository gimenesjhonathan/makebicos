@extends('layouts.app')
@section('content')
    <form action="{{url("/confirmar")}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="container">
            <div class="card  rounded-0 border-0 shadow mt-4">
                <div class="card-header">{{ __('Conclua Cadastro') }}</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3 mb-2 mt-2">
                            <input type="text"
                                   class="form-control{{ $errors->has('fone') ? ' is-invalid' : '' }}"
                                   name="fone" id="fone" maxlength="15" onkeydown="mascara( this )"
                                   onkeyup="mascara( this )" placeholder="Telefone" value="{{old('fone')}}">
                            @if ($errors->has('fone'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fone') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-md-3 mt-2">
                            <input class="form-control" placeholder="CEP" name="cep" type="text" id="cep" value="{{old('cep')}}" size="10" maxlength="9"
                                   onblur="pesquisacep(this.value);" onkeypress="return MM_formtCep(event,this,'#####-###');">
                        </div>
                        <div class="col-md-5 mb-2 mt-2">
                            <input type="text" id="cidade"
                                   class="form-control{{ $errors->has('cidade') ? ' is-invalid' : '' }}"
                                   name="cidade" placeholder="Cidade" value="{{old('cidade')}}">
                            @if ($errors->has('cidade'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cidade') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-md-1 mb-2 mt-2">
                            <input type="text" id="uf"
                                   class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}"
                                   name="estado" placeholder="UF" value="{{old('estado')}}">
                            @if ($errors->has('estado'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('estado') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-md-12 mb-2">
                            <input type="text" id="bairro" class="form-control{{ $errors->has('bairro') ? ' is-invalid' : '' }}" name="bairro" placeholder="Bairro"
                                   value="{{old('bairro')}}">
                            @if ($errors->has('bairro'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bairro') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-md-9 mb-2">
                            <input type="text" id="rua" class="form-control{{ $errors->has('logradouro') ? ' is-invalid' : '' }}" name="logradouro" placeholder="Logradouro"
                                   value="{{old('logradouro')}}">
                            @if ($errors->has('logradouro'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('logradouro') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-md-3 mb-2">
                            <input type="text" class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }}" name="numero" placeholder="Número"
                                   value="{{old('numero')}}">
                            @if ($errors->has('numero'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('numero') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <input name="ibge" type="text" id="ibge" hidden size="8" /></label><br />
                        <div class="col-md-12">
                            <div class="text-right">
                                <button type="submit" class="btn btn-success rounded-0 border-0 mb-2">Salvar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection