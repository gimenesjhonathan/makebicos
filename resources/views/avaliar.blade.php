@extends('layouts.app')
@section('content')
    <div class="container">
        @if(\Session::has('message'))
            <div class="alert alert-danger rounded-0 border-0 shadow mt-4" role="alert">
                <ul>
                    <li>{!! \Session::get('message') !!}</li>
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-6">
                <div class="card rounded-0 border-0 shadow">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h4 class="mb-5">@foreach($empresas as $empresa)
                                        {{$empresa->nome_empresa}}
                                    @endforeach</h4>
                            </div>
                            <div class="col-md-4">
                                @if($solicitacaoServicoClientes->estado == 'concluido')
                                <p class="text-right">Concluído</p>
                                @endif
                            </div>
                        </div>
                        <p class="text-justify">{{$solicitacaoServicoClientes->mensagem}}</p>
                        @if($solicitacaoServicoClientes->estado == 'concluido')
                            <form action="{{url("/avaliar/{$solicitacaoServicoClientes->id}/$solicitacaoServicoClientes->user_id")}}"
                                  method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="estrelas">
                                    <input type="radio" id="vazio" name="avaliacao" value="" checked>

                                    <label for="estrela_um"><i class="fa"></i></label>
                                    <input type="radio" id="estrela_um" name="avaliacao" value="1">

                                    <label for="estrela_dois"><i class="fa"></i></label>
                                    <input type="radio" id="estrela_dois" name="avaliacao" value="2">

                                    <label for="estrela_tres"><i class="fa"></i></label>
                                    <input type="radio" id="estrela_tres" name="avaliacao" value="3">

                                    <label for="estrela_quatro"><i class="fa"></i></label>
                                    <input type="radio" id="estrela_quatro" name="avaliacao" value="4">

                                    <label for="estrela_cinco"><i class="fa"></i></label>
                                    <input type="radio" id="estrela_cinco" name="avaliacao" value="5"><br><br>

                                    <input type="submit" class="btn btn-success" value="Avaliar">
                                </div>
                            </form>
                        @else
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card rounded-0 border-0 shadow">
                    <div class="card-body">
                        <h5 class="card-title">Avaliação</h5>

                        <p class="text-justify">Sua avaliação é muito importante para nós, assim podemos melhorar nossas
                            qualidades de serviços, avalie de forma sincera e muito obrigado!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function filtro() {
            window.location = ("/servicos-solicitados-" + document.getElementById("estado-servico").value);
        }
    </script>
@endsection