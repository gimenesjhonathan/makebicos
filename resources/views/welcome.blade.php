<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="/bulma-0.7.1/css/bulma.min.css">
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="navbar navbar-expand-md navbar-light navbar-laravel p-0 text-white"
     style="background-image: url('/img/home4.jpeg');">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}" class="text-white btn-danger">Home</a>
            @else
                <a href="{{ route('login') }}" class="text-white btn-danger">Entrar</a>
                <a href="{{ route('register') }}" class="text-white btn-danger">Criar conta</a>
            @endauth

            @endif
        </div>
        <div class="content w-100 mt-0 pt-0">
            <div class="rounded-0 border-0 shadow mt-0">
                <div class="p-5">
                    <div class="card-body" style="margin-top: 25rem; margin-bottom: 2rem;">
                        <h1 class="text-white text-center">Bem Vindo ao MakeBicos</h1>
                        <h4 class="text-white text-center">Nosso objetivo é gerar oportunidades</h4>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="container pb-5 mt-5">
    <div class="row">
        <div class="col-md-6">
            <img src="{{url('/img/fusca.jpg')}}">
        </div>
        <div class="col-md-6">
            <p class="text-justify">Desemprego é o maior reflexo da crise econômica que nosso país enfrenta hoje.

                Para uma pessoa conseguir arrumar um emprego é necessário que a oportunidade desse emprego exista, e
                isso é exatamente o que não está acontecendo.

                Devido à alta dos juros, inflação e falta de investimentos, o país parou de fabricar, produzir, vender e
                consumir.

                O Brasil hoje está com uma taxa de desemprego de 8,90%.

                Significa que 18,16 milhões de pessoas estão desempregadas.

                O que o governo tem feito para ajudar:

                Forneceu mais de 55 mil vistos para imigrantes se estabelecerem no Brasil;

                Cortou salários de funcionários públicos;

                Cancelou os concursos públicos;

                Acabou com financiamentos estudantis;

                Entre outros que prefino não falar para não gerar debates políticos

                Sabemos que esses profissionais desempregados possuem famílias e suas necessidades. O que você acha que
                um pai de família ao ver seu filho passando fome vai fazer? Prefiro nem responder a essa pergunta.

                Vamos ilustrar o quadro econômico do Brasil cruzando informações com a administração de nosso governo ao
                longo dos anos.</p>
            <p class="font-italic text-right">Wilton Moreira Junior</p>
        </div>
        <div class="col-md-6 mt-4" style="font-size: 1.6rem;">
            <p class="text-justify">A motivação para criação desse sistema é ajudar as pessoas a infretar essa crise, hoje
            varias pessoas tem dificuldades de encontrar pessoas para prestar alguns serviços expecificos, então tivemos
            a ideia de criar a ponte entre quem busca o prestador de serviço e o prestador de serviço assim gerando oportunidades
            para as pessoas que tenha dificuldades de encontrar trabalho em meio essa crise.</p>
        </div>
        <div class="col-md-6 mt-4">
            <img src="{{url('/img/solucao.jpeg')}}">
        </div>
    </div>
</div>
</body>
</html>
