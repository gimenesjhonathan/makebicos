@extends('layouts.app')
@section('content')
    <div class="container mt-4">
        @if(\Session::has('message'))
            <div class="alert alert-danger rounded-0 border-0 shadow mt-4" role="alert">
                <ul>
                    <li>{!! \Session::get('message') !!}</li>
                </ul>
            </div>
        @endif
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger rounded-0 border-0 shadow mb-2mt-2 m-0 border-0" role="alert">
                @foreach($errors->all() as $erro)
                    <p class="font-weight-bold">{{$erro}}</p>
                @endforeach
            </div>
        @endif
        @if (isset($item) != 0)
            <div class="alert alert-danger rounded-0 border-0 shadow mb-2 mt-2 m-0 border-0" role="alert">
                <p class="font-weight-bold">{{$item}}</p>
            </div>
        @endif
        <ul class="nav nav-tabs  mb-3" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link" href="{{url('/servicos')}}">Novos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/servicos-marcados')}}">Marcados</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/servicos-concluidos')}}">Concluídos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="{{url('/servicos-vencidos')}}">Vencidos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/servicos-descartados')}}">Descartados</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade  show active" id="vencido" role="tabpanel" aria-labelledby="contact-tab">
                <div class="alert alert-warning" role="alert">
                    <p>Os serviços foi vencido devido não ser atendido na data marcada, caso o serviço já tenha sido
                        atendido clique concluir ou podendo remarcar ou descartar o serviço.</p>
                </div>
                @foreach($solicitacaoServicos as $solicitacaoServico)
                    @php
                        $idSolicitacaoServico = $solicitacaoServico->id;
                        $userIdSolicitacaoServico = $solicitacaoServico->user_id;
                        $empresas = App\User::find($userIdSolicitacaoServico)->empresa;

                    @endphp
                    @if($solicitacaoServico->estado == 'vencido')
                        <div class="col-md-12 mb-4">
                            <div class="card rounded-0 border-0 shadow">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h4 class="mb-3">
                                                {{$solicitacaoServico->cliente}}
                                            </h4>
                                            <p class="mb-0">Telefone: {{$solicitacaoServico->telefone}}</p>
                                            <p class="mt-0">Endereço: {{$solicitacaoServico->endereco}}</p>
                                        </div>
                                        <div class="col-md-4">
                                            @if($solicitacaoServico->estado == 'vencido')
                                                <p class="text-right">Vencido</p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-md-9">
                                            <p>{{$solicitacaoServico->mensagem}}</p>
                                        </div>
                                        <div class="col-md-3">
                                            @php
                                                $database = strtotime($solicitacaoServico->created_at);
                                                $data = date ('d/m/Y', $database);
                                            @endphp
                                            <p class="text-right">{{$data}}</p>
                                        </div>
                                    </div>
                                    <div class="border-top">
                                        <div class="row mt-2 ml-2">
                                            <div class="col-md-2 m-0 p-0">
                                                <p>
                                                    <a data-toggle="collapse" role="button"
                                                       aria-expanded="false"
                                                       href="#concluir{{$solicitacaoServico->id}}"
                                                       aria-controls="multiCollapseExample1">Concluir</a>
                                                </p>
                                            </div>
                                            <div class="col-md-2 m-0 p-0">
                                                <p>
                                                    <a data-toggle="collapse"
                                                       href="#multiCollapseExample1{{$solicitacaoServico->id}}"
                                                       role="button"
                                                       aria-expanded="false" aria-controls="multiCollapseExample1">Remarcar</a>
                                                </p>
                                            </div>
                                            <div class="col-md-3 m-0 p-0">
                                                <p>
                                                    <a data-toggle="collapse" role="button"
                                                       href="#descartar{{$solicitacaoServico->id}}"
                                                       aria-expanded="false" aria-controls="multiCollapseExample1">Descartar
                                                        Serviço</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="collapse multi-collapse"
                                                 id="multiCollapseExample1{{$solicitacaoServico->id}}">
                                                <div class="card card-body">
                                                    <form action="{{url("/atender-servico/{$solicitacaoServico->id}")}}"
                                                          method="POST" enctype="multipart/form-data">
                                                        {{ method_field('PUT') }}
                                                        {{ csrf_field() }}
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="formGroupExampleInput">Data do
                                                                        Atendimento</label>
                                                                    <input type="date" name="dia"
                                                                           value="{{$solicitacaoServico->dia}}"
                                                                           class="form-control"
                                                                           id="formGroupExampleInput"
                                                                           placeholder="Example input">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="formGroupExampleInput">Data do
                                                                        Atendimento</label>
                                                                    <input type="time" name="horario"
                                                                           value="{{$solicitacaoServico->horario}}"
                                                                           class="form-control"
                                                                           id="formGroupExampleInput"
                                                                           placeholder="Example input">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 text-right">
                                                                <button type="submit" class="btn btn-success">
                                                                    Salvar
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="collapse multi-collapse"
                                                 id="concluir{{$solicitacaoServico->id}}">
                                                <div class="card card-body">
                                                    <form action="{{url("/concluir-servico/{$solicitacaoServico->id}")}}"
                                                          method="POST" enctype="multipart/form-data">
                                                        {{ method_field('PUT') }}
                                                        {{ csrf_field() }}
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p>Tem Certeza que deseja concluir esse serviço?</p>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <label for="valor">Valor</label>
                                                                        <input type="text" name="valor"
                                                                               class="dinheiro form-control"
                                                                               id="dinheiro"
                                                                               placeholder="R$ 0,00">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label for="valor">Despesas</label>
                                                                        <input type="text" name="despesa"
                                                                               class="dinheiro form-control"
                                                                               id="dinheiro"
                                                                               placeholder="R$ 0,00"
                                                                               onKeyPress="return(moeda(this,'.',',',event))">
                                                                    </div>
                                                                </div>
                                                                <div class="mt-2">
                                                                    <a class="btn btn-danger" data-toggle="collapse"
                                                                       role="button" style="color: #ffffff!important;"
                                                                       aria-expanded="false"
                                                                       href="#concluir{{$solicitacaoServico->id}}"
                                                                       aria-controls="multiCollapseExample1">Não</a>
                                                                    <button type="submit" class="btn btn-primary">
                                                                        Sim
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="collapse multi-collapse"
                                                 id="descartar{{$solicitacaoServico->id}}">
                                                <div class="card card-body">
                                                    <form action="{{url("/descartar-servico/{$solicitacaoServico->id}")}}"
                                                          method="POST" enctype="multipart/form-data">
                                                        {{ method_field('PUT') }}
                                                        {{ csrf_field() }}
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p>Tem Certeza que deseja descartar esse serviço?</p>
                                                                <input type="text" name="justificativa_descarte"
                                                                       class="form-control"
                                                                       id="despesa"
                                                                       placeholder="Justificativa do descarte...">
                                                                <div class="mt-2">
                                                                    <a class="btn btn-danger" data-toggle="collapse"
                                                                       role="button" style="color: #ffffff!important;"
                                                                       aria-expanded="false"
                                                                       href="#concluir{{$solicitacaoServico->id}}"
                                                                       aria-controls="multiCollapseExample1">Não</a>
                                                                    <button type="submit" class="btn btn-primary">
                                                                        Sim
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection