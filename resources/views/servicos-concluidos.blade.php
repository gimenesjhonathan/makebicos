@extends('layouts.app')
@section('content')
    <div class="container mt-4">
        @if(\Session::has('message'))
            <div class="alert alert-danger rounded-0 border-0 shadow mt-4" role="alert">
                <ul>
                    <li>{!! \Session::get('message') !!}</li>
                </ul>
            </div>
        @endif
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger rounded-0 border-0 shadow mb-2mt-2 m-0 border-0" role="alert">
                @foreach($errors->all() as $erro)
                    <p class="font-weight-bold">{{$erro}}</p>
                @endforeach
            </div>
        @endif
        @if (isset($item) != 0)
            <div class="alert alert-danger rounded-0 border-0 shadow mb-2 mt-2 m-0 border-0" role="alert">
                <p class="font-weight-bold">{{$item}}</p>
            </div>
        @endif
        <ul class="nav nav-tabs  mb-3" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link" href="{{url('/servicos')}}">Novos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/servicos-marcados')}}">Marcados</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="{{url('/servicos-concluidos')}}">Concluídos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/servicos-vencidos')}}">Vencidos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/servicos-descartados')}}">Descartados</a>
            </li>
        </ul>
        <div class="tab-pane fade show active" id="concluido" role="tabpanel" aria-labelledby="contact-tab">
            @foreach($solicitacaoServicos as $solicitacaoServico)
                @php
                    $idSolicitacaoServico = $solicitacaoServico->id;
                    $avaliacoes = App\SolicitacaoServico::find($idSolicitacaoServico)->avaliacao;
                    $userIdSolicitacaoServico = $solicitacaoServico->user_id;
                    $empresas = App\User::find($userIdSolicitacaoServico)->empresa;

                @endphp
                @if($solicitacaoServico->estado == 'concluido')
                    <div class="col-md-12 mb-4">
                        <div class="card rounded-0 border-0 shadow">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h4 class="mb-3">
                                            {{$solicitacaoServico->cliente}}
                                        </h4>
                                        <p class="mb-0">Telefone: {{$solicitacaoServico->telefone}}</p>
                                        <p class="mt-0">Endereço: {{$solicitacaoServico->endereco}}</p>
                                    </div>
                                    <div class="col-md-4">
                                        @if($solicitacaoServico->estado == 'concluido')
                                            <p class="text-right">Concluído</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <p>{{$solicitacaoServico->mensagem}}</p>
                                    </div>
                                    <div class="col-md-4 text-right">
                                        <p>Valor: R$ {{$solicitacaoServico->valor}}</p>
                                    </div>
                                </div>
                                @foreach($avaliacoes as $avaliacao)
                                    <div class="row">
                                        <div class="col-md-10">
                                            @if($avaliacao->avaliacao == 5)
                                                <img src="{{url('/img/avaliacao/avaliacao5.png')}}"
                                                     style="height: 2rem; width: 10rem;">
                                            @endif
                                            @if($avaliacao->avaliacao == 4)
                                                <img src="{{url('/img/avaliacao/avaliacao4.png')}}"
                                                     style="height: 2rem; width: 10rem;">
                                            @endif
                                            @if($avaliacao->avaliacao == 3)
                                                <img src="{{url('/img/avaliacao/avaliacao3.png')}}"
                                                     style="height: 2rem; width: 10rem;">
                                            @endif
                                            @if($avaliacao->avaliacao == 2)
                                                <img src="{{url('/img/avaliacao/avaliacao2.png')}}"
                                                     style="height: 2rem; width: 10rem;">
                                            @endif
                                            @if($avaliacao->avaliacao == 1)
                                                <img src="{{url('/img/avaliacao/avaliacao1.png')}}"
                                                     style="height: 2rem; width: 10rem;">
                                            @endif
                                            @php
                                                $database = strtotime($solicitacaoServico->created_at);
                                                $data = date ('d/m/Y', $database);
                                            @endphp
                                        </div>
                                        <div class="col-md-2">
                                            <p class="text-right">{{$data}}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
    <script>
        function filtro() {
            window.location = ("/servicos-solicitados-" + document.getElementById("estado-servico").value);
        }
    </script>
@endsection