@extends('layouts.app')
@section('content')
    <div class="container">
        <form action="{{url("/pesquisar")}}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="card  rounded-0 border-0 shadow mt-4">
                <div class="card-header">{{ __('Buscar') }}</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 mb-2">
                            <label for="categoria_id">Escolha a Categoria</label>
                            <select id="categoria_id" name="categoria_id" onchange="categoria()"
                                    class="form-control{{ $errors->has('categoria_id') ? ' is-invalid' : '' }}">
                                <option value="">Selecione Categoria</option>
                                @foreach($categorias as $categoria)
                                    @if($id == " ")
                                        <option value="{{$categoria->id}}">{{$categoria->categoria}}</option>
                                    @else
                                        @if($id == $categoria->id))
                                        <option value="{{$categoria->id}}" selected>{{$categoria->categoria}}</option>
                                        @else
                                            <option value="{{$categoria->id}}">{{$categoria->categoria}}</option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>
                            @if ($errors->has('categoria_id'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('categoria_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-md-6 mb-2">
                            <label for="categoria_id">Escolha o Serviço</label>
                            <select id="servico" name="servico"
                                    class="form-control{{ $errors->has('servico') ? ' is-invalid' : '' }}">
                                @if($servicos == " ")
                                    <option value="">Selecione Servico</option>
                                @else
                                    @foreach($servicos as $servico)
                                        <option value="{{$servico->servico}}">{{$servico->servico}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="container">
                            <div class="col-md-12 mt-5">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-danger rounded-0 border-0 mb-2">Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script>
        function categoria() {
            window.location = ("/buscar/categoria/" + document.getElementById("categoria_id").value);
        }
    </script>
@endsection