@extends('layouts.app')
@section('content')
    <div class="container mb-5 mt-4">
        @if(\Session::has('message'))
            <div class="alert alert-success rounded-0 border-0 shadow mt-4" role="alert">
                <ul>
                    <li>{!! \Session::get('message') !!}</li>
                </ul>
            </div>
        @endif
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger rounded-0 border-0 shadow mb-2mt-2 m-0 border-0" role="alert">
                @foreach($errors->all() as $erro)
                    <p class="font-weight-bold">{{$erro}}</p>
                @endforeach
            </div>
        @endif
        <div class="row">
            <div class="col-md-9">
                <div class="card rounded-0 border-0 shadow mb-3">
                    <div class="card-body">
                        <form action="{{url("/concluir-alterar/update/{$user->id}")}}" method="POST"
                              enctype="multipart/form-data">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <label for="inputName"
                                       class="form-label">{{ __('Nome') }}</label>
                                <input type="text"
                                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                       id="inputName" name="name"
                                       value="{{$user->name}}">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <label for="inputName"
                                       class="form-label">{{ __('Sobrenome') }}</label>
                                <input type="text"
                                       class="form-control{{ $errors->has('sobrenome') ? ' is-invalid' : '' }}"
                                       id="inputNomeSobrenome"
                                       name="sobrenome"
                                       value="{{$user->sobrenome}}">
                                @if ($errors->has('sobrenome'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('sobrenome') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12 mb-0 mt-4">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-success rounded-0 border-0">Alterar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card rounded-0 border-0 shadow">
                    <div class="card-body p-0">
                        @foreach($user->perfil as $perfil)
                            <form action="{{url("/avatar/{$userId}")}}" method="POST"
                                  enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <img id="imagem" src="{{ asset($perfil->image)}}" class="w-100"
                                     style="height: 10.2rem;">
                                <label class="btn btn-primary pt-0 pb-0 w-100 mt-0 rounded-0 border-0"
                                       style="margin-top: -18px;">Escolha a foto<input class="file-input"
                                                                                       type="file"
                                                                                       name="image" id="image"
                                                                                       onchange="previewImagem()"
                                                                                       hidden></label>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-success rounded-0 border-0 mr-2 mb-2">
                                        Alterar
                                    </button>
                                </div>
                            </form>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @php
            $i = 1;
        @endphp

        <div class="row">
            <div class="col-md-9">
                @php
                    $e = 1;
                @endphp
                @foreach($endereco as $enderec)
                    <form action="{{url("/concluir-alterar/update/{$enderec->id}")}}" method="POST"
                          enctype="multipart/form-data">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="card rounded-0 border-0 shadow mb-3">
                            <div class="card-body">
                                @if($e == 1)
                                    <label for="inputDdd"
                                           class="form-label">{{ __('Primeiro endereço') }}</label>
                                @endif
                                @if($e == 2)
                                    <label for="inputDdd"
                                           class="form-label">{{ __('Segundo endereço') }}</label>
                                @endif
                                @if($e == 3)
                                    <label for="inputDdd"
                                           class="form-label">{{ __('Terceiro endereço') }}</label>
                                @endif
                                <div class="row">
                                    <div class="col-md-6 mb-2">
                                        <label for="inputDdd"
                                               class="form-label">{{ __('Estado') }}</label>
                                        <select id="inputState" name="estado"
                                                class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}">
                                            <option selected>{{$enderec->estado}}</option>
                                            <option value="AC">Acre</option>
                                            <option value="AL">Alagoas</option>
                                            <option value="AP">Amapá</option>
                                            <option value="AM">Amazonas</option>
                                            <option value="BA">Bahia</option>
                                            <option value="CE">Ceará</option>
                                            <option value="DF">Distrito Federal</option>
                                            <option value="ES">Espírito Santo</option>
                                            <option value="GO">Goiás</option>
                                            <option value="MA">Maranhão</option>
                                            <option value="MT">Mato Grosso</option>
                                            <option value="MS">Mato Grosso do Sul</option>
                                            <option value="MG">Minas Gerais</option>
                                            <option value="PA">Pará</option>
                                            <option value="PB">Paraíba</option>
                                            <option value="PR">Paraná</option>
                                            <option value="PE">Pernambuco</option>
                                            <option value="PI">Piauí</option>
                                            <option value="RJ">Rio de Janeiro</option>
                                            <option value="RN">Rio Grande do Norte</option>
                                            <option value="RS">Rio Grande do Sul</option>
                                            <option value="RO">Rondônia</option>
                                            <option value="RR">Roraima</option>
                                            <option value="SC">Santa Catarina</option>
                                            <option value="SP">São Paulo</option>
                                            <option value="SE">Sergipe</option>
                                            <option value="TO">Tocantins</option>
                                        </select>
                                        @if ($errors->has('cidade'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors('estado') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 mb-2">
                                        <label for="inputDdd"
                                               class="form-label">{{ __('Cidade') }}</label>
                                        <input type="text"
                                               class="form-control{{ $errors->has('cidade') ? ' is-invalid' : '' }}"
                                               name="cidade"
                                               value="{{$enderec->cidade}}">
                                        @if ($errors->has('cidade'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors('cidade') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-md-12 mb-2">
                                        <label for="inputDdd"
                                               class="form-label">{{ __('Bairro') }}</label>
                                        <input type="text"
                                               class="form-control{{ $errors->has('bairro') ? ' is-invalid' : '' }}"
                                               name="bairro"
                                               value="{{$enderec->bairro}}">
                                        @if ($errors->has('bairro'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors('bairro') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-md-10 mb-2">
                                        <label for="inputDdd"
                                               class="form-label">{{ __('Logradouro') }}</label>
                                        <input type="text"
                                               class="form-control{{ $errors->has('logradouro') ? ' is-invalid' : '' }}"
                                               name="logradouro"
                                               value="{{$enderec->logradouro}}">
                                        @if ($errors->has('logradouro'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors('logradouro') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-md-2 mb-2">
                                        <label for="inputDdd"
                                               class="form-label">{{ __('Lote') }}</label>
                                        <input type="text"
                                               class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }}"
                                               name="numero"
                                               value="{{$enderec->numero}}">
                                        @if ($errors->has('numero'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('numero') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12 mb-0 mt-4 pr-0">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success rounded-0 border-0">Alterar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @php
                        $e++;
                    @endphp
                @endforeach
            </div>
            <div class="col-md-3">
                @foreach($telefone as $telefon)
                    <form action="{{url("/concluir-alterar/update/{$telefon->id}")}}" method="POST"
                          enctype="multipart/form-data">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}
                        <div class="card rounded-0 border-0 shadow mb-3">
                            <div class="card-body">
                                @if($i == 1)
                                    <label for="inputDdd"
                                           class="form-label">{{ __('Primeiro telefone') }}</label>
                                @endif
                                @if($i == 2)
                                    <label for="inputDdd"
                                           class="form-label">{{ __('Segundo telefone') }}</label>
                                @endif
                                @if($i == 3)
                                    <label for="inputDdd"
                                           class="form-label">{{ __('Terceiro telefone') }}</label>
                                @endif
                                <input type="text"
                                       class="form-control{{ $errors->has('fone') ? ' is-invalid' : '' }}"
                                       id="fone" name="fone" maxlength="15" onkeydown="mascara( this )"
                                       onkeyup="mascara( this )"
                                       value="{{$telefon->fone}}">
                                @if ($errors->has('fone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fone') }}</strong>
                                    </span>
                                @endif
                                <div class="col-md-12 mb-0 mt-4 pr-0">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success rounded-0 border-0">Alterar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @php
                        $i++;
                    @endphp
                @endforeach
            </div>
        </div>
    </div>
@endsection