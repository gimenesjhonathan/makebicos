@extends('layouts.app')
@section('content')
    <div class="container">
        @if($qtSolicitacaoNovas != 0)
        <div class="mt-4">
            <div class="alert alert-danger rounded-0 border-0 shadow mb-2 mt-2 m-0 border-0" role="alert">
                <p class="font-weight-bold"> Você tem {{$qtSolicitacaoNovas}} solicitação para prestação de serviço
                    <a href="{{url('/servicos')}}" class="btn btn-primary rounded-0 border-0 text-right" style="color: #ffffff !important;">Clique aqui
                    </a>
                </p>
            </div>
        </div>
        @endif
        @if($qtSolicitacaoHoje != 0)
            <div class="mt-4">
                <div class="alert alert-danger rounded-0 border-0 shadow mb-2 mt-2 m-0 border-0" role="alert">
                    <p class="font-weight-bold"> Você tem {{$qtSolicitacaoHoje}} solicitação de serviço para atender hoje
                        <a href="{{url('/servicos-marcados')}}" class="btn btn-primary rounded-0 border-0 text-right" style="color: #ffffff !important;">Clique aqui
                        </a>
                    </p>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-4 mt-4">
                <p class="text-justify" style="font-size: 1.2rem;">A MakeBicos ajuda você encontrar pessoas para
                    solucionar seus problema em casa. Não perca tempo busque alguém capaz para acabar com seus problemas
                    aqui na MakeBicos.</p>
                <a href="{{url("/buscar")}}" class="btn btn-danger rounded-0 border-0 mb-2" style="color: #ffffff;">Buscar
                    Serviço</a>
            </div>
            <div class="col-md-5  mt-4">
                <img src="{{url('/img/capa.jpeg')}}">
            </div>
            @if(count(\App\User::find($userId)->servico) == 0)
            <div class="col-md-3 mt-4">
                <p class="text-justify" style="font-size: 1.2rem;">Se você tem uma empresa ou até mesmo so preste
                    serviço, mas tem dificuldade para encontrar trabalho torna-se prestador de serviço na MakeBicos.</p>
                <a href="{{url("/concluirs")}}" class="btn btn-danger rounded-0 border-0 mb-2" style="color: #ffffff;">Cadastre
                    sua empresa</a>
            </div>
                @else
                <div class="col-md-3 mt-4">
                    <p class="text-justify" style="font-size: 1.2rem;">Use o gerencionamento para analisar lucros e despesas que está tendo ao decorrer do uso do sistema.</p>
                    <a href="{{url("/gerencionamento")}}" class="btn btn-danger rounded-0 border-0 mb-2" style="color: #ffffff;">Gerencionamento</a>
                </div>
            @endif
            <div class="col-md-6 mt-4">
                <img src="{{url('/img/imggrafico.jpeg')}}">
            </div>
            <div class="col-md-6 mt-4">
                <p class="text-justify" style="font-size: 1.3rem;">A empresa privada, é uma organização social orientada
                    para prestação de serviço
                    ou fabricação de determinado bem que vise atender a uma fatia específica do mercado. O lucro dentro
                    da
                    empresa privada é o resultado do esforço geral em todas áreas da Organização para atender às
                    necessidades do clientes, o satisfazendo e tendo em vista os custos e despesas que são envolvidos na
                    operação. Só isso? Não. Para o empreendedor que deseja abrir um negócio, é necessário entender os
                    aspectos que circundam o lucro, um item exaustivamente tratado no mercado. O MakeBicos ajuda as
                    empresas ter controle de seus lucros e custos ao decorrer de seus atendimentos.</p>
            </div>
        </div>
    </div>
@endsection
