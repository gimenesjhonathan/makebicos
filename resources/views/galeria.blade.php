@extends('layouts.app')
@section('content')
    @php
        $user = Auth::user();
        $userId = Auth::user()->id;
    @endphp
    <div class="container ">
        @if(\Session::has('message'))
            <div class="alert alert-danger rounded-0 border-0 shadow mt-4" role="alert">
                <ul>
                    <li>{!! \Session::get('message') !!}</li>
                </ul>
            </div>
        @endif
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger rounded-0 border-0 shadow mt-2 m-0 border-0" role="alert">
                @foreach($errors->all() as $erro)
                    <p class="font-weight-bold">{{$erro}}</p>
                @endforeach
            </div>
        @endif
        @if($id == Auth::User()->id)
        <div class="card rounded-0 border-0 shadow mt-4">
            <div class="card-body">
                <form action="{{url("/adicionar-foto")}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="gallery"></div>
                    <label class="btn btn-primary pt-0 pb-0 w-100 mt-0 rounded-0 border-0"
                           style="margin-top: -18px;">Adicionar foto na galeria<input name="foto[]" type="file" multiple id="gallery-photo-add" hidden></label>
                    <div class="text-right">
                        <a href="{{url("/galeria/{$id}")}}" class="btn btn-danger rounded-0 border-0 mb-2" style="color: #ffffff;">Cancelar</a>
                        <button type="submit" class="btn btn-success rounded-0 border-0 mb-2">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
        @else
        @endif
        <div class="card rounded-0 border-0 shadow mt-4">
            <div class="card-body">
                <div class="row">
                @foreach($galerias as $galeria)
                    <div class="col-md-4 p-2">
                        <img id="imagem" src="{{ asset($galeria->foto)}}" class="w-100" style="height: 15rem;">
                        <a data-toggle="collapse" href="#alterar{{$galeria->id}}" role="button" aria-expanded="false" aria-controls="collapseExample" class="btn btn-success rounded-0 border-0 shadow w-100 font-weight-bold p2 mt-0">
                            <p style="color: #ffffff">{{$galeria->nome_foto}}</p>
                        </a>
                        <div class="collapse btn btn-success rounded-0 border-0 shadow w-100" id="alterar{{$galeria->id}}">
                            <form action="{{url("/adicionar-foto/{$galeria->id}")}}" class="w-100" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-8">
                                        <input type="text" name="nome_foto" class="form-control rounded-0 border-0" placeholder="{{$galeria->nome_foto}}">
                                    </div>
                                    <div class="col-md-4">
                                        <button type="submit" class="btn btn-primary rounded-0 border-0 mr-2 mb-2">Salvar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                @endforeach
                </div>
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center mt-4">
                        {{$galerias->links()}}
                    </ul>
                </nav>
            </div>
        </div>
    </div>
@endsection