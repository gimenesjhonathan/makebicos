@extends('layouts.app')
@section('content')
    @if(count($telefone) == 0 | count($endereco) == 0 | count($servico) == 0)
        <div class="alert alert-warning" role="alert">
            Clique aqui para conluir seu cadastro, para poder já ser visto pelos seus futuros clientes <a
                    href="{{url("/concluir")}}" class="btn btn-outline-info">Concluir Cadastro</a>
        </div>
    @endif
    @if(count($user->galeria) == 0)
    @else
        <div class="row m-0">
            <div class="col-md-9 p-0">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @php
                            $i = '1';
                        @endphp
                        @if(count($user->galeria) == 0)
                            <div class="carousel-item @if($i == 1) active @else @endif">
                                <img class="d-block w-100 border border-white"
                                     src="{{url('/img/fotos/sem-foto-na-galeria.jfif')}}"
                                     style="height: 30rem;" alt="First slide">
                            </div>
                        @else
                            @foreach($user->galeria as $galeria)
                                <div class="carousel-item @if($i == 1) active @else @endif">
                                    <img class="d-block w-100 border border-white" src="{{ asset($galeria->foto)}}"
                                         style="height: 30rem;" alt="First slide">
                                </div>
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        @endif
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 p-0">
                @if(count($user->galeria) == 0)
                    <img class="d-block w-100 border border-white" src="{{url('/img/fotos/sem-foto-na-galeria.jfif')}}"
                         style="height: 10rem;" alt="Third slide">
                    <img class="d-block w-100 border border-white" src="{{url('/img/fotos/sem-foto-na-galeria.jfif')}}"
                         style="height: 10rem;" alt="Third slide">
                    <img class="d-block w-100 border border-white" src="{{url('/img/fotos/sem-foto-na-galeria.jfif')}}"
                         style="height: 10rem;" alt="Third slide">
                @endif
                @php
                    $e = '1';
                @endphp
                @foreach($user->galeria as $galeria)
                    @if($e < 4)
                        <img class="d-block w-100 border border-white" src="{{ asset($galeria->foto)}}"
                             style="height: 10rem;" alt="Third slide">
                    @endif
                    @php
                        $e++;
                    @endphp
                @endforeach
                @if(count($user->galeria) == 1)
                    <img class="d-block w-100 border border-white" src="{{url('/img/fotos/sem-foto-na-galeria.jfif')}}"
                         style="height: 10rem;" alt="Third slide">
                    <img class="d-block w-100 border border-white" src="{{url('/img/fotos/sem-foto-na-galeria.jfif')}}"
                         style="height: 10rem;" alt="Third slide">
                @endif
                @if(count($user->galeria) == 2)
                    <img class="d-block w-100 border border-white" src="{{url('/img/fotos/sem-foto-na-galeria.jfif')}}"
                         style="height: 10rem;" alt="Third slide">
                @endif
            </div>
        </div>
    @endif
    <div class="container">
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger rounded-0 border-0 shadow mt-2 m-0 border-0" role="alert">
                @foreach($errors->all() as $erro)
                    <p>{{$erro}}</p>
                @endforeach
            </div>
        @endif
        @if(\Session::has('message'))
            <div class="alert alert-danger rounded-0 border-0 shadow mt-4" role="alert">
                <ul>
                    <li>{!! \Session::get('message') !!}</li>
                </ul>
            </div>
        @endif
        <div class="row m-0">
            <div class="col col-md-3 mt-4 p-0">
                @foreach($user->perfil as $perfil)
                    <div class="text-center">
                        <img src="{{ asset($perfil->image)}}" class="w-100"
                             style="height: 15rem; width: 20rem;">
                    </div>
                @endforeach
                <div class="card border-0 shadow rounded-0 mt-3">
                    <div class="card-body">
                        @foreach($user->endereco as $endereco)
                            <div class="row">
                                <i class="fas fa-map-marker-alt ml-3 mt-1 mr-1"></i>
                                <p>{{$endereco->cidade}} - {{$endereco->estado}}</p>
                            </div>
                            @php
                                $servico = $user->servico;
                                $dia = $servico->first()->created_at;
                                $database = strtotime($dia);
                                $data = date ('d/m/Y', $database);
                            @endphp
                            <p>Presta Serviço na plataforma de {{$data}}</p>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col col-md-6 mt-4" style="padding-bottom: 10rem;">
                <div class="card border-0 shadow rounded-0">
                    <div class="card-body">
                        <h3>Olá, meu nome é {{$user->name}}</h3>
                        <div class="ml-4">
                            @foreach($user->servico as $servico)
                                <h5 class=" mt-3">{{$servico->servico}}</h5>
                                <p>{{$servico->desc_servico}}</p>
                                @if($userId == Auth::User()->id)
                                    <div class="row border-bottom ">
                                        <a class="pb-0 mb-0 font-weight-bold" data-toggle="collapse"
                                           href="#servico{{$servico->id}}" role="button" aria-expanded="false"
                                           aria-controls="collapseExample" style="color: #0f81cc!important;">Editar <i
                                                    class="fas fa-edit"></i></a>
                                        <a class="ml-2 pb-0 mb-0 font-weight-bold" data-toggle="collapse"
                                           href="#servicoex{{$servico->id}}" role="button" aria-expanded="false"
                                           aria-controls="collapseExample" style="color: #ff253a!important;">Excluir
                                            <i class="fas fa-trash-alt"></i></a>
                                    </div>
                                @else
                                @endif
                                <div class="collapse mt-2" id="servico{{$servico->id}}">
                                    <form action="{{url("/alterar-servico/{$servico->id}")}}" method="POST"
                                          enctype="multipart/form-data">
                                        {{ method_field('PUT') }}
                                        {{ csrf_field() }}
                                        <div class="col-md-12 mb-2">
                                            <label>Descrição do serviço</label>
                                            <textarea
                                                    class="form-control{{ $errors->has('desc_servico') ? ' is-invalid' : '' }}"
                                                    name="desc_servico" rows="6"
                                                    id="comment"
                                                    placeholder="Descreva sobre seu Serviço...">{{$servico->desc_servico}}
                                                </textarea>
                                            @if ($errors->has('desc_servico'))
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>
                                                            {{ $errors->first('desc_servico') }}
                                                        </strong>
                                                    </span>
                                            @endif
                                        </div>
                                        <div class="col-md-12">
                                            <div class="text-right">
                                                <button type="submit" class="btn btn-success rounded-0 border-0 mb-2">Salvar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="collapse mt-2" id="servicoex{{$servico->id}}">
                                    <form action="{{url("/deletar-servico/{$servico->id}")}}" method="POST"
                                          enctype="multipart/form-data">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <div class="col-md-12 mb-2">
                                            <p>Tem certeza que deseja excluir esse serviço</p>
                                        </div>
                                        <div class="col-md-12">
                                            <div>
                                                <button type="submit" class="btn btn-primary rounded-0 border-0 mb-2">Sim</button>
                                                <a data-toggle="collapse"
                                                   href="#servicoex{{$servico->id}}" role="button" aria-expanded="false"
                                                   aria-controls="collapseExample"  class="btn btn-danger rounded-0 border-0 mb-2" style="color: #FFFFFF!important;">Não</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endforeach
                        </div>
                        <p class="mt-3">
                            @if($userId == Auth::User()->id && count(\App\User::find($userId)->servico) < 3)
                                <a class="font-weight-bold" href="{{url('/concluirs')}}"
                                   style="color: #0f81cc!important;">
                                    Adicionar serviço
                                </a>
                            @else
                            @endif
                        </p>
                    </div>
                </div>
                <div class="card border-0 shadow rounded-0 mt-3">
                    <div class="card-body">
                        <h4>Comentários</h4>
                        <div class="border-bottom mt-1">
                            <p class="mt-2 row mb-0 ml-1">
                                <i class="far fa-comment mt-1"></i>
                                <a class="ml-1 font-weight-bold" data-toggle="collapse" href="#collapseExample"
                                   role="button"
                                   aria-expanded="false" aria-controls="collapseExample"
                                   style="color: #0f81cc!important;">
                                    Comentar
                                </a>
                            </p>
                            <div class="collapse mt-2 mb-2" id="collapseExample">
                                <form action="{{url("/comentar/{$userId}")}}" method="POST"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="mensagem" id="mensagem"
                                                       placeholder="Escreva um comentário...">
                                            </div>
                                        </div>
                                        <div class="col-md-3 p-0">
                                            <button type="submit" class="btn btn-success">Enviar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @foreach($comentarios as $comentario)
                            <blockquote class="blockquote mt-2">
                                @php
                                    $cliente = App\User::find($comentario->cliente_id)->name;
                                    $perfis = App\User::find($comentario->cliente_id)->perfil;
                                    $perfil->first()->image;
                                @endphp
                                <div class="row">
                                    <div class="col-md-2 text-center p-0">
                                        @foreach($perfis as $perfil)
                                            <img src={{ asset($perfil->image)}} class="rounded-circle"
                                                 style="height: 3rem; width: 3rem;">
                                        @endforeach

                                    </div>
                                    <div class="col-md-10 ml-0 p-0  border-bottom">
                                        <p class="m-0 font-weight-bold">{{$cliente}}</p>
                                        <p class="m-0">{{$comentario->mensagem}}</p>
                                    </div>
                                </div>
                            </blockquote>
                        @endforeach
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                {{$comentarios->links()}}
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="col col-md-3 p-0 mt-4">
                <a href="{{url("/galeria/{$userId}")}}">
                    <div class="card border-0 shadow rounded-0">
                        <div class="card-body btn btn-primary border-0 shadow rounded-0">
                            <p class="font-weight-bold font-italic mb-0">Galeria de fotos</p><i class="fas fa-camera"
                                                                                                style="font-size: 2rem;"></i>
                        </div>
                    </div>
                </a>
                <div class="card border-0 shadow rounded-0 mt-4">
                    <div class="card-body">
                        <h3>Avalições de clientes</h3>
                        @foreach($user->avaliacao->take(5) as $avaliacao)
                            <h5>{{$avaliacao->cliente}}</h5>
                            @if($avaliacao->avaliacao == 5)
                                <img src="{{url('/img/avaliacao/avaliacao5.png')}}"
                                     style="height: 2rem; width: 10rem;">
                            @endif
                            @if($avaliacao->avaliacao == 4)
                                <img src="{{url('/img/avaliacao/avaliacao4.png')}}"
                                     style="height: 2rem; width: 10rem;">
                            @endif
                            @if($avaliacao->avaliacao == 3)
                                <img src="{{url('/img/avaliacao/avaliacao3.png')}}"
                                     style="height: 2rem; width: 10rem;">
                            @endif
                            @if($avaliacao->avaliacao == 2)
                                <img src="{{url('/img/avaliacao/avaliacao2.png')}}"
                                     style="height: 2rem; width: 10rem;">
                            @endif
                            @if($avaliacao->avaliacao == 1)
                                <img src="{{url('/img/avaliacao/avaliacao1.png')}}"
                                     style="height: 2rem; width: 10rem;">
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="padding-bottom: 10rem;">

    </div>
    <div class="fixed-bottom">
        <div class="w-100">
            <div class="text-right p-3 border-top" style="background-color: #ffffff;">
                <div class="row">
                    <div class="col-md-8 m-0">
                        <div class="row pl-2">
                            <div class="col-md-2 text-center p-0">
                                @foreach($user->perfil as $perfil)
                                    <img src={{ asset($perfil->image)}} class="rounded-circle"
                                         style="height: 4rem; width: 4rem;">
                                @endforeach
                            </div>
                            <div class="col-md-6 p-0 text-left">
                                @foreach($user->empresa as $empresa)
                                    <p class="font-weight-bold mt-2 mb-0 ml-2">{{$empresa->nome_empresa}}</p>
                                @endforeach
                                <div class="mt-0">
                                    @if($media >= 4.9)
                                        <img src="{{url('/img/avaliacao/avaliacao5.png')}}"
                                             style="height: 2rem; width: 10rem;">
                                    @endif
                                    @if($media >= 4.5 && $media <= 4.8)
                                        <img src="{{url('/img/avaliacao/avaliacao4.5.png')}}"
                                             style="height: 2rem; width: 10rem;">
                                    @endif
                                    @if($media >= 4 && $media <= 4.4)
                                        <img src="{{url('/img/avaliacao/avaliacao4.png')}}"
                                             style="height: 2rem; width: 10rem;">
                                    @endif
                                    @if($media >= 3.5 && $media <= 3.9)
                                        <img src="{{url('/img/avaliacao/avaliacao3.5.png')}}"
                                             style="height: 2rem; width: 10rem;">
                                    @endif
                                    @if($media >= 3 && $media <= 3.4)
                                        <img src="{{url('/img/avaliacao/avaliacao3.png')}}"
                                             style="height: 2rem; width: 10rem;">
                                    @endif
                                    @if($media >= 2.5 && $media <= 2.9)
                                        <img src="{{url('/img/avaliacao/avaliacao2.5.png')}}"
                                             style="height: 2rem; width: 10rem;">
                                    @endif
                                    @if($media >= 2 && $media <= 2.4)
                                        <img src="{{url('/img/avaliacao/avaliacao2.png')}}"
                                             style="height: 2rem; width: 10rem;">
                                    @endif
                                    @if($media >= 1.5 && $media <= 1.9)
                                        <img src="{{url('/img/avaliacao/avaliacao1.5.png')}}"
                                             style="height: 2rem; width: 10rem;">
                                    @endif
                                    @if($media >= 1 && $media <= 1.4)
                                        <img src="{{url('/img/avaliacao/avaliacao1.png')}}"
                                             style="height: 2rem; width: 10rem;">
                                    @endif
                                    @if($media >= 0.5 && $media <= 0.9)
                                        <img src="{{url('/img/avaliacao/avaliacao0.5.png')}}"
                                             style="height: 2rem; width: 10rem;">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 m-0 mt-3">
                        @if($userId != Auth::User()->id)
                            <a class="btn btn-danger" href="{{url("/solicitar/{$userId}")}}"
                               style="color: #ffffff!important;">Solicitar
                                Serviço</a>
                        @else
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
