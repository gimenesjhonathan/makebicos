@extends('layouts.app')
@section('content')
    <div class="container mt-4">
        @if(\Session::has('message'))
            <div class="alert alert-success" role="alert">
                <ul>
                    <li>{!! \Session::get('message') !!}</li>
                </ul>
            </div>
        @endif
        @foreach($solicitacaoServicoClientes as $solicitacaoServico)
            @if($solicitacaoServico->estado == 'vencido' || $solicitacaoServico->estado == 'descartado')
            @else
                @php
                    $idUser = $solicitacaoServico->user_id;
                    $perfis = \App\User::find($idUser)->perfil;
                    $idSolicitacaoServico = $solicitacaoServico->id;
                    $avaliacoes = App\SolicitacaoServico::find($idSolicitacaoServico)->avaliacao;

                @endphp
                <div class="col-md-12 mb-4">
                    <div class="card rounded-0 border-0 shadow
                    @if($solicitacaoServico->estado == 'novo') bg-danger @endif
                    @if($solicitacaoServico->estado == 'marcado') bg-success @endif
                    @if($solicitacaoServico->estado == 'concluido')bg-primary @endif
                            ">
                        <div class="card-body  text-white">
                            <div class="row">
                                <div class="col-md-2 p-0">
                                    @php
                                        $idEmpresa = $solicitacaoServico->user_id;
                                        $name = App\User::find($idEmpresa)->name;
                                    @endphp
                                    @foreach($perfis as $perfil)
                                        <div class="text-center">
                                            <img src={{ asset($perfil->image)}} class="rounded-circle"
                                                 style="height: 7rem; width: 7rem;">
                                        </div>
                                    @endforeach
                                    <p class="text-center">{{$name}}</p>
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h4>
                                                @php
                                                    $idEmpresa = $solicitacaoServico->user_id;
                                                    $empresas = App\User::find($idEmpresa)->empresa;
                                                @endphp
                                                @foreach($empresas as $empresa)
                                                    {{$empresa->nome_empresa}}
                                                @endforeach
                                            </h4>
                                        </div>

                                    </div>
                                    @php
                                        $telefones = App\User::find($idEmpresa)->telefone;
                                        $t = 1;
                                    @endphp
                                    @foreach($telefones as $telefone)
                                        @if($t == 1)
                                            <p>Telefone: {{$telefone->fone}}</p>
                                        @endif
                                        @php
                                            $t++;
                                        @endphp
                                    @endforeach
                                    <div class="row mt-4">
                                        <div class="col-md-8">
                                            <p>{{$solicitacaoServico->mensagem}}</p>
                                        </div>
                                    </div>
                                    @if($solicitacaoServico->estado == 'marcado')
                                        @php
                                            $database = strtotime($solicitacaoServico->dia);
                                            $dataMarcada = date ('d/m/Y', $database);
                                        @endphp
                                        <div class="alert alert-warning" role="alert">
                                            <h4>Atendimento marcado para {{$dataMarcada}}
                                                ás {{$solicitacaoServico->horario}}</h4>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    <div>
                                        @if($solicitacaoServico->estado == 'novo')
                                            <p class="text-right font-weight-bold">Aguarde...</p>
                                        @endif
                                        @if($solicitacaoServico->estado == 'marcado')
                                            <p class="text-right">Marcado</p>
                                        @endif
                                        @if($solicitacaoServico->estado == 'concluido')
                                            <p class="text-right">Concluído</p>
                                        @endif
                                        <div>
                                            @php
                                                $database = strtotime($solicitacaoServico->created_at);
                                                $data = date ('d/m/Y', $database);
                                            @endphp
                                            <p class="text-right">{{$data}}</p>
                                            @if($solicitacaoServico->estado == 'novo')
                                                <p class="text-right" style="font-size: 1.5rem;">
                                                    <a data-toggle="collapse" role="button"
                                                       style="color: #ffffff !important;"
                                                       href="#descartar{{$solicitacaoServico->id}}"
                                                       aria-expanded="false" aria-controls="multiCollapseExample1">Excluir
                                                        <i
                                                                class="fas fa-trash-alt"></i></a>
                                                </p>
                                            @endif
                                        </div>
                                        @foreach($avaliacoes as $avaliacao)
                                            <div class="row text-right">
                                                <div class="col-md-12 mb-2 text-right">
                                                    @if($avaliacao->avaliacao == 5)
                                                        <img src="{{url('/img/avaliacao_servico/avaliacao5.png')}}"
                                                             style="height: 2rem; width: 10rem;">
                                                    @endif
                                                    @if($avaliacao->avaliacao == 4)
                                                        <img src="{{url('/img/avaliacao_servico/avaliacao4.png')}}"
                                                             style="height: 2rem; width: 10rem;">
                                                    @endif
                                                    @if($avaliacao->avaliacao == 3)
                                                        <img src="{{url('/img/avaliacao_servico/avaliacao3.png')}}"
                                                             style="height: 2rem; width: 10rem;">
                                                    @endif
                                                    @if($avaliacao->avaliacao == 2)
                                                        <img src="{{url('/img/avaliacao_servico/avaliacao2.png')}}"
                                                             style="height: 2rem; width: 10rem;">
                                                    @endif
                                                    @if($avaliacao->avaliacao == 1)
                                                        <img src="{{url('/img/avaliacao_servico/avaliacao1.png')}}"
                                                             style="height: 2rem; width: 10rem;">
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                        @if($solicitacaoServico->estado == 'concluido')
                                            @if(count($avaliacoes) != 0)
                                            @else
                                                <div class="text-right">
                                                    <p>
                                                        <a style="color: #ffffff !important;"
                                                           href="{{url("/avaliar/{$solicitacaoServico->id}")}}">Avaliar</a>
                                                    </p>
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="collapse multi-collapse"
                                         id="descartar{{$solicitacaoServico->id}}">
                                        <div class="card card-body">
                                            <form action="{{url("/excluir-servico/{$solicitacaoServico->id}")}}"
                                                  method="POST" enctype="multipart/form-data">
                                                {{ method_field('delete') }}
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p>Tem Certeza que deseja excluir essa solicitação de
                                                            serviço?</p>
                                                        <div class="mt-2">
                                                            <a class="btn btn-danger" data-toggle="collapse"
                                                               role="button"
                                                               style="color: #ffffff!important;"
                                                               aria-expanded="false"
                                                               href="#concluir{{$solicitacaoServico->id}}"
                                                               aria-controls="multiCollapseExample1">Não</a>
                                                            <button type="submit" class="btn btn-primary">
                                                                Sim
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center mb-4">
                {{$solicitacaoServicoClientes->links()}}
            </ul>
        </nav>
    </div>
    <script>
        function filtro() {
            window.location = ("/servicos-solicitados-" + document.getElementById("estado-servico").value);
        }
    </script>
@endsection