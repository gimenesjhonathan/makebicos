@extends('layouts.app')
@section('content')
    <div class="container mb-4">
        <div class="row mt-4">
            <div class="col-md-4">
                <div class="card text-white bg-success mb-3 rounded-0 border-0 shadow">
                    <div class="card-header">Valor bruto - Mensal</div>
                    <div class="card-body">
                        <p class="card-title text-center" style="font-size: 3rem; height: 6rem;">
                            R$ {{$replaceValorMesAtual}}</p>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-white bg-danger mb-3 rounded-0 border-0 shadow">
                    <div class="card-header">Despesas - Mensal</div>
                    <div class="card-body">
                        <p class="card-title text-center" style="font-size: 3rem; height: 6rem;">
                            R$ {{$replaceDespesaMesAtual}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-white bg-info mb-3 rounded-0 border-0 shadow">
                    <div class="card-header">Valor líquido - Mensal</div>
                    <div class="card-body">
                        <p class="card-title text-center" style="font-size: 3rem; height: 6rem;">
                            R$ {{$replaceLucrosMesAtual}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card rounded-0 border-0 shadow">
                    <div class="card-body">
                        <div id="perf_div"></div>
                        <?= $lava->render('ColumnChart', 'Finances', 'perf_div') ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="card rounded-0 border-0 shadow mt-3"
             style="background-image: url('/img/dinheiro.jpg'); background-size: 70rem;">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card text-white bg-warning mb-3 rounded-0 border-0 shadow">
                            <div class="card-header">Ganho total com MakeBicos</div>
                            <div class="card-body">
                                <p class="card-title text-center" style="font-size: 3rem; height: 6rem;">
                                    R$ {{$replaceValorLucroComSistema}}</p>

                            </div>
                        </div>
                        <div class="card text-white bg-secondary rounded-0 border-0 shadow">
                            <div class="card-header">Pessoas atendidas</div>
                            <div class="card-body">
                                <p class="card-title text-center" style="font-size: 3rem; height: 6rem;">
                                     {{$pessoasAtendidas}} <i class="fas fa-user"></i></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card text-white bg-dark mb-3 rounded-0 border-0 shadow">
                            <img class="card-img-top rounded-0 border-0" src="{{url('/img/moeda.jpg')}}" alt="Card image cap" style="height: 15.6rem;">
                            <div class="card-body">
                                <p class="card-title text-justify mb-0">
                                    Com um pouco de trabalho qualquer um consegue fazer dinheiro. Agora, para fazer
                                    muito
                                    dinheiro, é necessário fazer a diferença. “O
                                    dinheiro
                                    segue a paixão, não o contrário”.</p>
                                <p class="text-right font-italic text-white mt-4 mb-0">Tom Brokaw</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card rounded-0 border-0 shadow mt-3">
            <div class="card-body">
                <div class="text-right"><a href="{{url('/pdf')}}"><p class="font-weight-bold" style="font-size: 1.5rem;">Gerar PDF <i class="fas fa-file-pdf"></i></p></a></div>
                <table class="table">
                    <thead class="bg-info rounded-0 border-0 shadow mt-3">
                    <tr>
                        <th scope="col" style="color: #ffffff;">Cliente</th>
                        <th scope="col" style="color: #ffffff;">Descrição Servico</th>
                        <th scope="col" style="color: #ffffff;">Valor</th>
                        <th scope="col" style="color: #ffffff;">Data / Horário</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($solicitacaoServicos as $solicitacaoServico)
                    <tr>
                            @php
                                //$teste = date('d-m-y', $solicitacaoServico->dia);
                               $data = date("d/m/Y", strtotime($solicitacaoServico->dia))
                            @endphp
                            <th scope="row">{{$solicitacaoServico->cliente}}</th>
                            <td>{{$solicitacaoServico->servico}}</td>
                            <td>R$ {{str_replace('.', ',', $solicitacaoServico->valor)}}</td>
                            <td>{{$data}} / {{$solicitacaoServico->horario}}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection