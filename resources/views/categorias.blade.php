@extends('layouts.app')
@section('content')
    <div class="container">
        @if(\Session::has('message'))
            <div class="alert alert-danger rounded-0 border-0 shadow mt-4" role="alert">
                <ul>
                    <li>{!! \Session::get('message') !!}</li>
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-6 mt-4">
                <div class="card  rounded-0 border-0 shadow mt-4">
                    <div class="card-body">
                        <h3 class="border-bottom p-1">Cadastrar Categorias</h3>
                        <form action="{{url("/cadastrar-categoria")}}" method="POST"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="text"
                                   class="form-control"
                                   name="categoria" placeholder="Categoria" value="{{old('categoria')}}">
                            <div class="text-right mt-2">
                                <button type="submit" class="btn btn-success rounded-0 border-0 mb-2">
                                    Salvar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-4">
                <div class="card  rounded-0 border-0 shadow mt-4">
                    <div class="card-body">
                        <h3 class="border-bottom p-1">Cadastrar Serviços</h3>
                        <form action="{{url("/cadastrar-servico")}}" method="POST"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <select id="categoria_id" name="categoria_id"
                                            class="form-control{{ $errors->has('categoria_id') ? ' is-invalid' : '' }}">
                                        <option value="">Selecione Categoria</option>
                                        @foreach($categorias as $categoria)
                                            @if($id == " ")
                                                <option value="{{$categoria->id}}">{{$categoria->categoria}}</option>
                                            @else
                                                @if($id == $categoria->id))
                                                <option value="{{$categoria->id}}"
                                                        selected>{{$categoria->categoria}}</option>
                                                @else
                                                    <option value="{{$categoria->id}}">{{$categoria->categoria}}</option>
                                                @endif
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="text"
                                           class="form-control"
                                           name="servico" placeholder="Servicos" value="{{old('servico')}}">
                                </div>
                                <div class="col-md-12 mt-2">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success rounded-0 border-0 mb-2">Salvar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection