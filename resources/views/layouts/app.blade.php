<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="bg-light">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MakeBicos') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/fontawesome-free-5.3.1-web/css/all.min.css">
    <link rel="stylesheet" href="/bulma-0.7.1/css/bulma.min.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <style>
        body {
            margin: 0px;
            padding: 0px;
        }

        .perfil {
            width: 7rem;
            height: 7rem;
        }

        .capa {
            width: 100%;
            height: 10rem;
        }

        .card-footer {
            background: #EC7063 !important;

        }

        .card-footer .text-muted {
            color: #fff !important;
        }

        a:link {
            color: black !important;
            text-decoration: none
        }

        /* link que foi visitado */
        a:visited {
            color: black;
        !important;
        }

        p {
            font-size: 1rem;
        }


        .estrelas input[type=radio]{
            display: none;
        }.estrelas label i.fa:before{
             content: '\f005';
             color: #FC0;
         }.estrelas  input[type=radio]:checked  ~ label i.fa:before{
              color: #CCC;
          }


    </style>
</head>
<body class="bg-light">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel p-0 bg-info text-white">
            <div class="container">
                <a class="navbar-brand text-white" href="{{ url('/') }}">
                    MakeBicos
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto ">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Entrar') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{ route('register') }}">{{ __('Criar conta') }}</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{url('/home')}}">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{url('/buscar')}}">Buscar Serviço</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white" href="{{url('/denuncias-sugestoes')}}">Denuncia / Sugestão</a>
                            </li>
                            <li class="nav-item dropdown text-white">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="color: #f7f7f7!important;">
                                    Servicos<span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right bg-info" aria-labelledby="navbarDropdown">
                                    <a class="nav-link text-white" href="{{url('/servicos-solicitados')}}">Serviços Solicitados</a>
                                    @if(count(Auth::user()->servico) != 0)
                                    <a class="nav-link text-white" href="{{url('/servicos')}}">Meus Serviços</a>
                                    <a class="nav-link text-white" href="{{url('/gerencionamento')}}">Gerenciamento</a>
                                    @endif
                                </div>
                            </li>
                            <li class="nav-item dropdown bg-info">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle bg-info" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="color: #f7f7f7!important;">
                                    @php
                                        $userId = Auth::user()->id;
                                        $servico = Auth::user()->servico;
                                        $perfil = Auth::user()->perfil;
                                        if(count($perfil) != 0){
                                            $image = $perfil->first()->image;
                                        }
                                    @endphp
                                    @if(count($perfil) == 0)
                                        {{Auth::user()->name}} <span class="caret"></span>
                                    @endif
                                    @if(isset($image) && $image == 'users/043750201809175b9f2f9e307a8.jpg')
                                        {{Auth::user()->name}} <span class="caret"></span>
                                        @else
                                        @foreach(Auth::user()->perfil as $perfil)
                                            <img class="card-img-top perfil rounded-circle shadow" src="{{ asset($perfil->image) }}" style="width: 2rem; height: 2rem;"> <span class="caret"></span>
                                        @endforeach
                                    @endif
                                </a>
                                <div class="dropdown-menu dropdown-menu-right bg-info" aria-labelledby="navbarDropdown">

                                    @if(count($servico) == 0)
                                    @else
                                    <a class="nav-link text-white" href="{{url("/perfil")}}">
                                        Seu Perfil
                                    </a>
                                    @endif
                                    <a class="nav-link text-white" href="{{url("/concluir-alterar/{$userId}/edit")}}" style="color: #FFFFFF !important;">
                                        Editar Perfil
                                    </a>
                                    <a class="nav-link text-white" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Sair
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <main class="p-0 m-0">
            @yield('content')
        </main>
    </div>
    <script
            src="http://code.jquery.com/jquery-2.1.0.min.js"
            integrity="sha256-8oQ1OnzE2X9v4gpRVRMb1DWHoPHJilbur1LP9ykQ9H0="
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>
</body>
<script>
    $('.dinheiro').mask('###0,00', {reverse: true});

    function previewImagem(){
        var image = document.querySelector('input[name=image]').files[0];
        var preview = document.getElementById('imagem');

        var reader = new FileReader();

        reader.onloadend = function () {
            preview.src = reader.result;
        }

        if(image){
            reader.readAsDataURL(image);
        }else{
            preview.src = "";
        }
    }

    stop = '';
    function mascara( campo ) {
        campo.value = campo.value.replace( /[^\d]/g, '' )
            .replace( /^(\d\d)(\d)/, '($1) $2' )
            .replace( /(\d{4,5})(\d)/, '$1-$2' );
        if ( campo.value.length > 15 ) campo.value = stop;
        else stop = campo.value;
    }

    function descartar() {
        function envio() {
            var r = confirm("Tem certeza que deseja enviar as informações?");
            if (r == true) {
                teste.submit();
            }
        }
    }

    function moeda(a, e, r, t) {
        let n = ""
            , h = j = 0
            , u = tamanho2 = 0
            , l = ajd2 = ""
            , o = window.Event ? t.which : t.keyCode;
        if (13 == o || 8 == o)
            return !0;
        if (n = String.fromCharCode(o),
        -1 == "0123456789".indexOf(n))
            return !1;
        for (u = a.value.length,
                 h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
            ;
        for (l = ""; h < u; h++)
            -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
        if (l += n,
        0 == (u = l.length) && (a.value = ""),
        1 == u && (a.value = "0" + r + "0" + l),
        2 == u && (a.value = "0" + r + l),
        u > 2) {
            for (ajd2 = "",
                     j = 0,
                     h = u - 3; h >= 0; h--)
                3 == j && (ajd2 += e,
                    j = 0),
                    ajd2 += l.charAt(h),
                    j++;
            for (a.value = "",
                     tamanho2 = ajd2.length,
                     h = tamanho2 - 1; h >= 0; h--)
                a.value += ajd2.charAt(h);
            a.value += r + l.substr(u - 2, u)
        }
        return !1
    }

    $(function() {
        // Multiple images preview in browser
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        $($.parseHTML('<img style="width: 8rem; height: 8rem; margin: 0.5rem;">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#gallery-photo-add').on('change', function() {
            imagesPreview(this, 'div.gallery');
        });
    });



    function limpa_formulário_cep() {
        //Limpa valores do formulário de cep.
        document.getElementById('rua').value=("");
        document.getElementById('bairro').value=("");
        document.getElementById('cidade').value=("");
        document.getElementById('uf').value=("");
        document.getElementById('ibge').value=("");
    }

    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
            //Atualiza os campos com os valores.
            document.getElementById('rua').value=(conteudo.logradouro);
            document.getElementById('bairro').value=(conteudo.bairro);
            document.getElementById('cidade').value=(conteudo.localidade);
            document.getElementById('uf').value=(conteudo.uf);
            document.getElementById('ibge').value=(conteudo.ibge);
        } //end if.
        else {
            //CEP não Encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
        }
    }

    function pesquisacep(valor) {

        //Nova variável "cep" somente com dígitos.
        var cep = valor.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                document.getElementById('rua').value="...";
                document.getElementById('bairro').value="...";
                document.getElementById('cidade').value="...";
                document.getElementById('uf').value="...";
                document.getElementById('ibge').value="...";

                //Cria um elemento javascript.
                var script = document.createElement('script');

                //Sincroniza com o callback.
                script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';
                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);

            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    };

    function MM_formtCep(e,src,mask) {
        if(window.event) { _TXT = e.keyCode; }
        else if(e.which) { _TXT = e.which; }
        if(_TXT > 47 && _TXT < 58) {
            var i = src.value.length; var saida = mask.substring(0,1); var texto = mask.substring(i)
            if (texto.substring(0,1) != saida) { src.value += texto.substring(0,1); }
            return true; } else { if (_TXT != 8) { return false; }
        else { return true; }
        }
    }
</script>
</html>
