@extends('layouts.app')
@section('content')
    <form action="{{url("/solicitar/{$id}/cadastrarsolicitacao")}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="container">
            @if(\Session::has('message'))
                <div class="alert alert-danger rounded-0 border-0 shadow mt-4" role="alert">
                    <ul>
                        <li>{!! \Session::get('message') !!}</li>
                    </ul>
                </div>
            @endif
            @if (isset($errors) && count($errors) > 0)
                <div class="alert alert-danger rounded-0 border-0 shadow mt-2 m-0 border-0" role="alert">
                    @foreach($errors->all() as $erro)
                        <p class="font-weight-bold">{{$erro}}</p>
                    @endforeach
                </div>
            @endif
            <div class="row">
                <div class="col-md-8">
                    <div class="card rounded-0 border-0 shadow mt-4">
                        <div class="card-body">
                            <h3 class="border-bottom p-1">Solicitar Serviço</h3>
                            <div class="row mt-2">
                                <div class="col-md-12">
                                    <label>Escolha o Telefone</label>
                                    <div class="row">
                                        @foreach($telefones as $telefone)
                                            <div class="col-md-8">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text" style="background-color: #0f81cc">
                                                            <input type="radio" name="telefone" value="{{$telefone->fone}}"
                                                                   aria-label="Checkbox for following text input" value="1" @if(Input::old('telefone')) checked @endif>
                                                        </div>
                                                    </div>
                                                    <input type="text" class="form-control" disabled
                                                           value="{{$telefone->fone}}"
                                                           aria-label="Text input with checkbox"
                                                           style="background-color: #0f81cc; color: #ffffff;">
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="col-md-2">
                                            <a data-toggle="modal" data-target="#telefone"><i
                                                        class="fas fa-plus-square"
                                                        style="font-size: 2.5rem; color: #0f81cc;"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label>Escolha o Endereço</label>
                                    <div class="row">
                                        @foreach($enderecos as $endereco)
                                            <div class="col-md-10">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text" style="background-color: #0f81cc">
                                                        <input type="radio" name="ende"
                                                               aria-label="Radio button for following text input" value="1" @if(Input::old('ende')) checked @endif>
                                                    </div>
                                                    <input type="text" class="form-control" name="endereco"
                                                           value="{{$endereco->bairro}} {{$endereco->logradouro}} {{$endereco->lote}} {{$endereco->cidade}} - {{$endereco->estado}}"
                                                           aria-label="Text input with radio button"
                                                           style="background-color: #0f81cc; color: #ffffff;">
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="col-md-2">
                                            <a data-toggle="modal" data-target="#endereco"><i
                                                        class="fas fa-plus-square"
                                                        style="font-size: 2.5rem; color: #0f81cc;"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <select id="servico" name="servico"
                                            class="form-control mt-2{{ $errors->has('servico') ? ' is-invalid' : '' }}">
                                        @foreach($servicos as $servico)
                                            <option value="{{$servico->servico}}">{{$servico->servico}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12">
                                <textarea class="form-control mt-2"
                                          name="mensagem" rows="6"
                                          id="comment"
                                          placeholder="Descreva sobre os serviços que necessita...">{{old('desc_servico')}}</textarea>
                                    @if ($errors->has('desc_servico'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('desc_servico') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="row mt-2">
                                    <a href="{{url('/home')}}" class="btn btn-danger"
                                       style="color: #ffffff;">Cancelar</a>
                                    <button type="submit" class="ml-2 btn btn-success">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card  rounded-0 border-0 shadow mt-4">
                        <div class="card-body">
                            <h3>Informação</h3>
                            <p>Marque o Telefone e o Endereço que deseja o atendimento.</p>
                            <p>Escolha o Serviço que deseja atendimento.</p>
                            <p>Descreva sobre o serviço.</p>
                            <p>Essas informações são essenciais para a agilidade do atendimento.</p>
                            <p>Caso queira incluir alguma telefone ou endereço novo clique em adicionar <a href="#"><i
                                            class="fas fa-plus-square mt-2"
                                            style="font-size: 2.5rem; color: #0f81cc;"></i></a>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Modal Adicionar Telefone-->
    <div class="modal fade" id="telefone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content rounded-0 border-0 shadow">
                <div class="modal-header">
                    <h5 class="modal-title" id="telefone">Adicionar Telefone</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{url("/adicionar-telefone")}}" method="POST"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="fone">Telefone</label>
                            <input type="text" class="form-control{{ $errors->has('fone') ? ' is-invalid' : '' }}"
                                   id="fone"
                                   name="fone" maxlength="15" onkeydown="mascara( this )" onkeyup="mascara( this )"
                                   placeholder="(00) 00000-0000">
                        </div>
                        <div class="text-right">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Adicionar Endereço-->
    <div class="modal fade" id="endereco" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content rounded-0 border-0 shadow">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Adicionar Endereço</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{url("/adicionar-endereco")}}" method="POST"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6 mb-2">
                                <select id="inputState" name="estado"
                                        class="form-control{{ $errors->has('estado') ? ' is-invalid' : '' }}">
                                    <option value="">Selecione o Estado...</option>
                                    <option value="AC">Acre</option>
                                    <option value="AL">Alagoas</option>
                                    <option value="AP">Amapá</option>
                                    <option value="AM">Amazonas</option>
                                    <option value="BA">Bahia</option>
                                    <option value="CE">Ceará</option>
                                    <option value="DF">Distrito Federal</option>
                                    <option value="ES">Espírito Santo</option>
                                    <option value="GO">Goiás</option>
                                    <option value="MA">Maranhão</option>
                                    <option value="MT">Mato Grosso</option>
                                    <option value="MS">Mato Grosso do Sul</option>
                                    <option value="MG">Minas Gerais</option>
                                    <option value="PA">Pará</option>
                                    <option value="PB">Paraíba</option>
                                    <option value="PR">Paraná</option>
                                    <option value="PE">Pernambuco</option>
                                    <option value="PI">Piauí</option>
                                    <option value="RJ">Rio de Janeiro</option>
                                    <option value="RN">Rio Grande do Norte</option>
                                    <option value="RS">Rio Grande do Sul</option>
                                    <option value="RO">Rondônia</option>
                                    <option value="RR">Roraima</option>
                                    <option value="SC">Santa Catarina</option>
                                    <option value="SP">São Paulo</option>
                                    <option value="SE">Sergipe</option>
                                    <option value="TO">Tocantins</option>
                                </select>
                                @if ($errors->has('estado'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('estado') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 mb-2">
                                <input type="text"
                                       class="form-control{{ $errors->has('cidade') ? ' is-invalid' : '' }}"
                                       name="cidade" placeholder="Cidade" value="{{old('cidade')}}">
                                @if ($errors->has('cidade'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cidade') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12 mb-2">
                                <input type="text" class="form-control" name="bairro" placeholder="Bairro"
                                       value="{{old('bairro')}}">
                            </div>
                            <div class="col-md-9 mb-2">
                                <input type="text" class="form-control" name="logradouro" placeholder="Logradouro"
                                       value="{{old('logradouro')}}">
                            </div>
                            <div class="col-md-3 mb-2">
                                <input type="text" class="form-control" name="numero" placeholder="Número"
                                       value="{{old('numero')}}">
                            </div>
                        </div>
                        <div class="text-right mt-2">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection